/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 151);
/******/ })
/************************************************************************/
/******/ ({

/***/ 151:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(41);


/***/ }),

/***/ 41:
/***/ (function(module, exports) {

$(function () {
	// Binds to the global ajax scope
	$(document).ajaxStart(function () {
		$("#ajaxLoading").show();
	});

	$(document).ajaxComplete(function () {
		$("#ajaxLoading").hide();
	});
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$('.ajaxModal').click(function (e) {
		e.preventDefault();
		ajaxModal(this);
	});
	$('body').on('click', '.ajaxForm', function (e) {
		e.preventDefault();
		ajaxForm(this);
	});
        $('body').on('click', '.controlEnvironment', function (e) {
		e.preventDefault();
		$.post($(this).attr('href'), {}, function(response){
                    if(response.status == 'success'){
                        bootbox.alert('Environment Controlled Successfully.');
                        window.location = window.location;
                    }
                }, 'json');
	});
	$('body').on('click', '.dirTreeFolder', function (e) {
		e.preventDefault();
		if ($('#' + $(this).data('subid')).is(':empty')) {
			getDirTree($(this));
		}
		$('.fa', this).toggleClass('fa-caret-right').toggleClass('fa-caret-down');
	});
	$('body').on('click', '.dirTreeSelectFolder', function (e) {
		var folder = $(this).parents('.dirTreeFolder').first();
		var active = folder.hasClass('active');
		var icon = $(this).children('.fa');
		var input = $(this).parents('form').first().find('.dirTreeValue');
		var heading = $(this).parents('.card').first().find('.dirTreeHeading');
		$('.dirTreeIcon').removeClass('fa-minus-square');
		$('.dirTreeIcon').addClass('fa-check-square');
		icon.removeClass('fa-check-square');
		icon.removeClass('fa-minus-square');
		$('.list-group-item.active').removeClass('active');
		if (!active) {
			icon.addClass('fa-minus-square');
			folder.addClass('active');
			input.val(folder.data('directory'));
			heading.text(folder.data('windowsdir'));
		} else {
			icon.addClass('fa-check-square');
			input.val('');
			heading.text('Directory Tree');
		}
		folder.scrollCenter();
		e.stopPropagation();
	});

	$('body').on('click', '.environmentSetup', function (e) {
		e.preventDefault();
		$(this).runSetup();
	});
});
jQuery.fn.runSetup = function () {
	var url = this.attr('href');
	var dialog = bootbox.dialog({
		title: 'Setup Staus',
		message: '<p class="text-center setupLoading"><i class="fa fa-spin fa-spinner"></i> <span id="setupStatusCurrentMsg">Running Setup...</span></p><div id="setupStatus"></div>',
		closeButton: false
	});
	$.post(url, {}, function (response) {});
	var statusInterval = setInterval(getStatus, 500);
	var statusLink = this.data('statuslink');
	function getStatus() {
		$.get(statusLink, {}, function (response) {
			$('#setupStatus').html(response.html);
			$('#setupStatusCurrentMsg').html(response.currentMsg);
			if (response.status != 'running') {
				clearInterval(statusInterval);
				$('.setupLoading').hide();
                                window.location.reload();
			}
		}, 'json');
	}
	return this;
};
jQuery.fn.scrollCenter = function () {
	var speed = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 700;

	var el = this;
	var elOffset = el.offset().top;
	var elHeight = el.height();
	var windowHeight = $(window).height();
	var offset;

	if (elHeight < windowHeight) {
		offset = elOffset - (windowHeight / 2 - elHeight / 2);
	} else {
		offset = elOffset;
	}
	$('html, body').animate({ scrollTop: offset }, speed);
	return this;
};
function getDirTree(ele) {
	var directory = ele.data('directory');
	$.get('/treeview', { startDir: directory, htmlId: ele.data('subid') }, function (response) {
		$('#' + ele.data('subid')).remove();
		$(response).insertAfter(ele);
	});
}

function ajaxContent(ele, replaceDiv) {
	var link = $(ele);
	replaceDiv = $(replaceDiv);
	$.get(link.attr('href'), {}, function (response) {
		replaceDiv.html(response);
	});
}
function ajaxModal(ele) {
	var link = $(ele);
	var dialog = bootbox.dialog({
		title: link.attr('Title'),
		message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>'
	});
	dialog.init(function () {
		$.get(link.attr('href'), {}, function (response) {
			dialog.find('.bootbox-body').html(response);
		});
	});
}
function ajaxForm(ele) {
	var link = $(ele);

	$.post(link.attr('href'), link.parents('form').first().serialize(), function (response) {
		if (response.status == 'success') {
			bootbox.alert(response.msg);
			setTimeout(function () {
				location.reload();
			}, 1000);
		} else {
			$('.is-invalid').removeClass('is-invalid');
			var form = link.parents('form').first();
			form.addClass('was-validated');
			$.each(response.errFlds, function (fld, msg) {
				var input = form.find('input[name="' + fld + '"],select[name="' + fld + '"],textarea[name="' + fld + '"]');
				input.addClass('is-invalid');
				input.parents('.form-group').first().addClass('is-invalid');
			});
		}
	}, 'json');
}

/***/ })

/******/ });