<?php
/**
 * Created by PhpStorm.
 * User: dukes
 * Date: 6/23/2018
 * Time: 7:34 AM
 */
require __DIR__.'/../vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Turn On The Lights
|--------------------------------------------------------------------------
|
| We need to illuminate PHP development, so let us turn on the lights.
| This bootstraps the framework and gets it ready for use, then it
| will load up this application so that we can run it and send
| the responses back to the browser and delight our users.
|
*/

$app = require_once __DIR__.'/../bootstrap/app.php';
ini_set('display_errors',1);
error_reporting(E_ALL);
$host_entry = '172.16.0.1 wingstogo.test';
$out = shell_exec("sudo bash -c 'printf \"\n{$host_entry}\" >> /etc/hosts' 2>&1");
echo 'done';