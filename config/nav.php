<?php
return [
	['text'=>'Dashboard', 'link'=>'home', 'icon'=>'home'],
	['text'=>'Environments', 'link'=>'environments.index', 'icon'=>'database'],
	[
		['text'=>'Projects', 'link'=>'projects.index', 'icon'=>'globe'],
		['text'=>'Add Project', 'link'=>'projects.create', 'modal'=>true],
		['text'=>'Types', 'link'=>'projects.types'],
		['text'=>'Config Files', 'link'=>'projects.configfiles']
	],
	[
		['text'=>'Seo', 'link'=>'seo.index', 'icon'=>'chart-line', 'hidden'=>true],
		['text'=>'Link Title Checker', 'link'=>'seo.linkChecker'],
		['text'=>'Image Title Checker', 'link'=>'seo.imgChecker']
	],
	
	['text'=>'Settings', 'link'=>'settings.index', 'icon'=>'gear']
];