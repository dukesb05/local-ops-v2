<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectsDomainsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects_domains', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('project_id')->unsigned()->index('fk_projects_domain_project_id_idx');
			$table->integer('environment_id')->unsigned()->index('fk_projects_domain_environment_id_idx');
			$table->string('domain')->nullable();
			$table->integer('main')->nullable()->default(0);
			$table->integer('ssl')->nullable()->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projects_domains');
	}

}
