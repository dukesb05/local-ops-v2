<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('group_id')->unsigned()->index('fk_projects_group_id_idx');
			$table->integer('type_id')->unsigned()->index('fk_projects_type_id_idx');
			$table->integer('repo_id')->unsigned();
			$table->string('name')->nullable();
			$table->string('folder')->nullable();
			$table->string('repo_url')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projects');
	}

}
