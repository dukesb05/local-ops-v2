<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEnvironmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('environments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('abbrev', 3)->nullable();
			$table->string('ip_address', 45)->nullable();
			$table->text('apache_mods', 65535)->nullable();
			$table->enum('php_version', array('5.2','5.6','7.0','7.1','7.2'))->nullable();
			$table->text('php_extensions', 65535)->nullable();
			$table->enum('mysql_version', array('5.6','5.7'))->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('environments');
	}

}
