<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProjectsTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('projects_types', function(Blueprint $table)
		{
			$table->foreign('default_env', 'fk_projects_type_default_env_id_idx')->references('id')->on('environments')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('projects_types', function(Blueprint $table)
		{
			$table->dropForeign('fk_projects_type_default_env_id_idx');
		});
	}

}
