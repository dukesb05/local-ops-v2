<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProjectsDomainsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('projects_domains', function(Blueprint $table)
		{
			$table->foreign('project_id', 'fk_projects_domain_project_id_idx')->references('id')->on('projects')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('projects_domains', function(Blueprint $table)
		{
			$table->dropForeign('fk_projects_domain_project_id_idx');
		});
	}

}
