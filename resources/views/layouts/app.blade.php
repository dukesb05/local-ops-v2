<!DOCTYPE html>
<html lang="en">

	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
		<link rel="manifest" href="/site.webmanifest">
		<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#ffffff">

		<title>Local OPS</title>
		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="{{ mix('/css/app.css') }}">
	</head>

	<body>
		<div class="container-scroller">
			<!-- partial:../partials/_navbar.html -->
			<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
				<div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
					<a class="navbar-brand brand-logo" href="/">Local OPS</a>
					<a class="navbar-brand brand-logo-mini" href="/"><i class="fa fa-wrench"></i></a>
				</div>
				<div class="navbar-menu-wrapper d-flex align-items-center">
					<button class="navbar-toggler navbar-toggler d-none d-lg-block navbar-dark align-self-center mr-3" type="button" data-toggle="minimize">
						<span class="navbar-toggler-icon"></span>
					</button>
					<form class="form-inline mt-2 mt-md-0 d-none d-lg-block" style="width:30%;">
						<a id="projectSearchCreate" href="/projects/create" class="hidden"></a>
						{!! Form::select('search', [], null, ['class' => 'form-control mr-sm-2 search', 'id'=>'search']) !!}
					</form>
					<ul class="navbar-nav ml-lg-auto d-flex align-items-center flex-row">
						<li class="nav-item">
							<a class="nav-link" href="#"><i class="fa fa-wrench"></i></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#"><i class="fa fa-th"></i></a>
						</li>
					</ul>
					<button class="navbar-toggler navbar-dark navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
						<span class="navbar-toggler-icon"></span>
					</button>
				</div>
			</nav>
			<!-- partial -->
			<div class="container-fluid page-body-wrapper">
				<!-- partial:../partials/_sidebar.html -->
				<nav class="sidebar sidebar-offcanvas" id="sidebar">
					<ul class="nav">

						@foreach (config('nav') as $nav)
						<li class="nav-item">
							@if (isset($nav[0]))
							<a class="nav-link" data-toggle="collapse" href="#{{$nav[0]['text']}}" aria-expanded="false" aria-controls="{{$nav[0]['text']}}">
								<i class="fa fa-{{$nav[0]['icon']}} menu-icon"></i>
								<span class="menu-title">{{$nav[0]['text']}} <i class="fa fa-caret-down mx-auto"></i></span>
							</a>
							<div class="collapse" id="{{$nav[0]['text']}}">
								<ul class="nav flex-column sub-menu">
									@foreach ($nav as $subNav)
									@if (empty($subNav['hidden']))
									<li class="nav-item">
										<a class="nav-link{{ !empty($subNav['modal']) ? ' ajaxModal' : '' }}" href="{{ route("{$subNav['link']}") }}">{{$subNav['text']}}</a>
									</li>
									@endif
									@endforeach
								</ul>
							</div>
							@else
							<a class="nav-link{{ !empty($nav['modal']) ? ' ajaxModal' : '' }}" href="{{ route("{$nav['link']}") }}">
								<i class="menu-icon fa fa-{{$nav['icon']}}"></i>
								<span class="menu-title">{{$nav['text']}}</span>
							</a>
							@endif
						</li>
						@endforeach
					</ul>
				</nav>
				<!-- partial -->
				<div class="main-panel content-wrapper">
					<div class="content-wrapper">
						@yield('content')
					</div>
					<!-- content-wrapper ends -->
					<!-- partial:../partials/_footer.html -->
					<footer class="footer">
						<div class="container-fluid clearfix">
							<span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © <?php echo date('Y'); ?> <a href="http://127.0.0.1/">LOPS</a>. All rights reserved.</span>
						</div>
					</footer>
					<!-- partial -->
				</div>
				<!-- main-panel ends -->
			</div>
			<!-- page-body-wrapper ends -->
		</div>
		<!-- container-scroller -->
		<div id="ajaxLoading">
			<i class="fa fa-cog fa-spin fa-5x"></i>
		</div>
		<!-- Bootstrap core JavaScript -->
		<script src="{{ mix('/js/app.js') }}"></script>
		@yield('page-js-script')
		<script>
jQuery(function ($) {
    $('#search').select2({
        placeholder: 'Search...',
        minimumInputLength: 3,
        ajax: {
            url: '/projects/gitlabSearch',
            dataType: 'json'
        },
        width: '100%'
    });
    $('#search').on('select2:select', function (e) {
        var data = e.params.data;
        if (data.local_id) {
            window.location.href = '/projects/' + data.local_id;
        } else {
            var ele = $('#projectSearchCreate');
            ele.attr('href', ele.attr('href') + '?repo_id=' + data.id);
            var link = $(ele);
            var dialog = bootbox.dialog({
                title: link.attr('Title'),
                message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
                size: link.data('size') ? link.data('size') : null
            });
            dialog.init(function () {
                $.get(link.attr('href'), {}, function (response) {
                    dialog.find('.bootbox-body').html(response);
                });
            });
        }
    });
});
        </script>
	</body>

</html>
