<!DOCTYPE html>
<html lang="en">

	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>Star Admin Free Bootstrap-4 Admin Dashboard Template</title>
		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="{{ mix('/css/app.css') }}">
	</head>

	<body>
		<div class="container-scroller">
			<!-- partial:../partials/_navbar.html -->
			<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
				<div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
					<a class="navbar-brand brand-logo" href="/">Local OPS</a>
					<a class="navbar-brand brand-logo-mini" href="/"><i class="fa fa-wrench"></i></a>
				</div>
				<div class="navbar-menu-wrapper d-flex align-items-center">
					<button class="navbar-toggler navbar-toggler d-none d-lg-block navbar-dark align-self-center mr-3" type="button" data-toggle="minimize">
						<span class="navbar-toggler-icon"></span>
					</button>
					<form class="form-inline mt-2 mt-md-0 d-none d-lg-block">
						<input class="form-control mr-sm-2 search" type="text" placeholder="Search">
					</form>
					<ul class="navbar-nav ml-lg-auto d-flex align-items-center flex-row">
						<li class="nav-item">
							<a class="nav-link" href="#"><i class="fa fa-wrench"></i></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#"><i class="fa fa-th"></i></a>
						</li>
					</ul>
					<button class="navbar-toggler navbar-dark navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
						<span class="navbar-toggler-icon"></span>
					</button>
				</div>
			</nav>
			<!-- partial -->
			<div class="container-fluid page-body-wrapper">
				<!-- partial:../partials/_sidebar.html -->
				<nav class="sidebar sidebar-offcanvas" id="sidebar">
					<ul class="nav">
						
						@foreach (config('nav') as $nav)
						<li class="nav-item">
							@if (isset($nav[0]))
							<a class="nav-link" data-toggle="collapse" href="#{{$nav[0]['text']}}" aria-expanded="false" aria-controls="{{$nav[0]['text']}}">
								<i class="fa fa-{{$nav[0]['icon']}} menu-icon"></i>
								<span class="menu-title">{{$nav[0]['text']}} <i class="fa fa-caret-down mx-auto"></i></span>
							</a>
							<div class="collapse" id="{{$nav[0]['text']}}">
								<ul class="nav flex-column sub-menu">
									@foreach ($nav as $subNav)
									<li class="nav-item">
										<a class="nav-link" href="{{ route("{$subNav['link']}") }}">{{$subNav['text']}}</a>
									</li>
									@endforeach
								</ul>
							</div>
							@else
							<a class="nav-link" href="{{ route("{$nav['link']}") }}">
								<i class="menu-icon fa fa-{{$nav['icon']}}"></i>
								<span class="menu-title">{{$nav['text']}}</span>
							</a>
							@endif
						</li>
						@endforeach
					</ul>
				</nav>
				<!-- partial -->
				<div class="main-panel content-wrapper">
					<div class="content-wrapper">
						@yield('content')
					</div>
					<!-- content-wrapper ends -->
					<!-- partial:../partials/_footer.html -->
					<footer class="footer">
						<div class="container-fluid clearfix">
							<span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 <a href="http://www.bootstrapdash.com/" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
							<span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
						</div>
					</footer>
					<!-- partial -->
				</div>
				<!-- main-panel ends -->
			</div>
			<!-- page-body-wrapper ends -->
		</div>
		<!-- container-scroller -->
		<div id="ajaxLoading">
			<i class="fa fa-cog fa-spin fa-5x"></i>
		</div>
		<!-- Bootstrap core JavaScript -->
		<script src="{{ mix('/js/app.js') }}"></script>
	</body>

</html>
