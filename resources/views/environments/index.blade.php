@extends('layouts.app')

@section('content')
    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <h1 class="my-4">Environments
            <small><a class="btn btn-primary ajaxModal" href="{{ route("environments.create") }}" data-size="large">Add Environment</a>
            </small>
        </h1>
        @if (count($environments) > 0)
            <table class="table table-striped table-bordered table-hover">
                <thead class="thead-dark">
                <tr>
                    <th>Name</th>
                    <th>Apache</th>
                    <th>Ip Address</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($environments as $environment)
                    <tr>
                        <td><a href="{{ $environment->link() }}">{{ $environment->name }}</a></td>
                        <td><span class="stopped">{{ $environment->apacheIsRunning() ? 'Running' : 'Stopped' }}</span>
                        </td>
                        <td>{{ $environment->ip_address }}</td>
                        <td>
                            <a href="{{ $environment->link() }}" title="View" class="btn btn-sm btn-secondary"><i
                                        class="fa fa-eye"></i> View</a>
                            @if(!$environment->isInstalled())
                                <a href="{{ $environment->link('setup') }}"
                                   class="btn btn-sm btn-secondary" title="Stop"><i
                                            class="fa fa-cogs"></i> Setup</a>
                            @elseif ($environment->isRunning())
                                <a href="{{ $environment->link('control', ['action'=>'stop']) }}"
                                   class="btn btn-sm btn-secondary controlEnvironment" title="Stop"><i
                                            class="fa fa-power-off"></i> Stop</a>
                            @else
                                <a href="{{ $environment->link('control', ['action'=>'start']) }}"
                                   class="btn btn-sm btn-secondary controlEnvironment" title="Start"><i
                                            class="fa fa-play"></i> Start</a>
                            @endif
                            <a href="{{ $environment->link('control', ['action'=>'terminal']) }}"
                               class="btn btn-sm btn-secondary controlEnvironment" title="Show Terminal"><i
                                        class="fa fa-terminal"></i> Terminal</a>
                            @if($environment->isInstalled())
                                <a href="{{ $environment->link('control', ['action'=>'uninstall']) }}" title="View" class="btn btn-sm btn-secondary btn-danger controlEnvironment"
                                   data-confirm="Are you sure you want to uninstall {{ $environment->name }} Environment?">
                                    <i class="fa fa-remove"></i> Uninstall</a>
                            @endif
                            <a href="{{ $environment->link('control', ['action'=>'remove']) }}" title="View" class="btn btn-sm btn-secondary btn-danger controlEnvironment"
                               data-confirm="Are you sure you want to remove {{ $environment->name }} Environment?">
                                <i class="fa fa-trash"></i> Remove</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <!-- /.row -->
        @else
            <p class="alert alert-info">There are no Environments set up yet.</p>
        @endif
    </div>
    <!-- /.container -->
@endsection