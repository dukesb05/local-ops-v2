{!! Form::model($environment, ['route' => ['environments.update', $environment->id], 'class'=>'needs-validation']) !!}
<h3>Environment Info</h3>
<div class="form-group">
    {!! Form::label('name', 'Name', ['class' => 'control-label', 'required'=>true]) !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required'=>true]) !!}
    <div class="invalid-feedback">
        Please enter a Name.
    </div>
</div>
<div class="form-group">
    {!! Form::label('abbrev', 'Icon Text (max 3 chars)', ['class' => 'control-label', 'required'=>true]) !!}
    {!! Form::text('abbrev', null, ['class' => 'form-control', 'required'=>true]) !!}
    <div class="invalid-feedback">
        Please enter the text for the icon.
    </div>
</div>
<div class="form-group">
    {!! Form::label('apache_mods', 'Apache Modules', ['class' => 'control-label', 'required'=>false]) !!} <br/>
    {!! Form::checkboxes('apache_mods[]', \App\Environment\Setup\Apache::availableMods(),\App\Environment\Setup\Apache::$defaultMods, ['class' => 'form-check-input', 'required'=>true]) !!}
    <div class="invalid-feedback">
        Please select the Php version for the environment.
    </div>
</div>
<div class="form-group">
    {!! Form::label('php_version', 'PHP Version', ['class' => 'control-label', 'required'=>true]) !!}
    {!! Form::select('php_version', array_combine(\App\Environment\Setup\Php::versions(),\App\Environment\Setup\Php::versions()),'5.6', ['class' => 'form-control', 'required'=>true]) !!}
    <div class="invalid-feedback">
        Please select the Php version for the environment.
    </div>
</div>
<div class="form-group">
    {!! Form::label('php_extensions', 'PHP Extensions', ['class' => 'control-label', 'required'=>false]) !!} <br/>
    {!! Form::checkboxes('php_extensions[]', \App\Environment\Setup\Php::extensions('5.6'),\App\Environment\Setup\Php::defaultExtensions('5.6'), ['class' => 'form-check-input', 'required'=>true]) !!}
    <div class="invalid-feedback">
        Please select the Php version for the environment.
    </div>
</div>
<div class="form-group">
    {!! Form::label('mysql_version', 'Mysql Version', ['class' => 'control-label', 'required'=>true]) !!}
    {!! Form::select('mysql_version', array_combine(\App\Environment\Setup\Mysql::versions(),\App\Environment\Setup\Mysql::versions()),'5.6', ['class' => 'form-control', 'required'=>true]) !!}
    <div class="invalid-feedback">
        Please select the Php version for the environment.
    </div>
</div>
<div class="form-group">
    {!! Form::submit('Save Environment',['class' => 'btn btn-primary ajaxForm']) !!}
</div>
{!! Form::close() !!}
<script>
    $(function(){
       $('#php_version').change(function(e){

       });
    });
</script>