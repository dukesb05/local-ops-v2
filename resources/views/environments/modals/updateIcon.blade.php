{!! Form::model($environment, ['route' => ['environments.control', $environment->id, 'updateIcon'], 'class'=>'needs-validation']) !!}
<div class="form-group">
    {!! Form::label('abbrev', 'Icon Text (max 3 chars)', ['class' => 'control-label', 'required'=>true]) !!}
    {!! Form::text('abbrev', null, ['class' => 'form-control', 'required'=>true]) !!}
    <div class="invalid-feedback">
        Please enter the text for the icon.
    </div>
</div>
<div class="form-group">
    {!! Form::submit('Update Icon',['class' => 'btn btn-primary ajaxForm']) !!}
</div>
{!! Form::close() !!}