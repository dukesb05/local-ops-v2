@extends('layouts.app')

@section('content')
<!-- Page Content -->
<div class="container">

	<!-- Page Heading -->
	<h1 class="my-4">Environments - 
		<small>Setup {{$environment->name}}</small>
	</h1>
	<div class="well col-12">
		<a href="{{ $environment->link('setup') }}" data-statuslink='{{ $environment->link('setupStatus') }}' class="btn btn-primary environmentSetup col-12">Setup Environment</a>
	</div>
	<ul class="list-group list-group-root well">
		@foreach($environment->setupClass()->getSteps() as $step)
		<li class="list-group-item">{{ucwords(str_replace('_', ' ', $step))}}</li>
		@endforeach
	</ul>
</div>
<!-- /.container -->
@endsection