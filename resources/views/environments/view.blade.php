@extends('layouts.app')

@section('content')
    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <h1 class="my-4">
            <a href="{{route('environments.index')}}" class="btn btn-sm btn-secondary"><i class="fa fa-arrow-left"></i></a> Environments -
            <small>{{$environment->name}}</small>
        </h1>
        <div class="card">
            <h3 class="card-header">Actions</h3>
            <div class="card-body">
                <a href="{{$environment->link('control', ['action'=>'start'])}}" class="btn btn-secondary controlEnvironment{{$environment->isRunning() ? " disabled" : '' }}" title='Start'><i class="fa fa-play"></i>Start</a>
                <a href="{{$environment->link('control', ['action'=>'stop'])}}" class="btn btn-secondary controlEnvironment{{!$environment->isRunning() ? " disabled" : '' }}" title='Stop'><i
                            class="fa fa-power-off"></i>Stop</a>
                <a href="{{$environment->link('control', ['action'=>'restart'])}}" class="btn btn-secondary controlEnvironment{{!$environment->isRunning() ? " disabled" : '' }}" title="Restart"><i
                            class="fa fa-sync"></i>Restart</a>
                <a href="{{$environment->link('controlModal', ['action'=>'updateIcon'])}}" class="btn btn-secondary ajaxModal" title="Update Icon"><i
                            class="fa fa-pencil-square"></i>Update Icon</a>
                <a href="{{ $environment->link('control', ['action'=>'terminal']) }}" class="btn btn-sm btn-secondary controlEnvironment" title="Show Terminal"><i
                            class="fa fa-terminal"></i> Terminal</a>
                @if($environment->isInstalled())
                    <a href="{{ $environment->link('control', ['action'=>'uninstall']) }}" title="View" class="btn btn-sm btn-secondary btn-danger controlEnvironment"
                       data-confirm="Are you sure you want to uninstall {{ $environment->name }} Environment?">
                        <i class="fa fa-remove"></i> Uninstall</a>
                @endif
                <a href="{{ $environment->link('control', ['action'=>'remove']) }}" title="View" class="btn btn-sm btn-secondary btn-danger controlEnvironment"
                   data-confirm="Are you sure you want to remove {{ $environment->name }} Environment?">
                    <i class="fa fa-trash"></i> Remove</a>
            </div>
        </div>
        <div class="card">
            <h3 class="card-header">Environment Status</h3>
            <div class="card-body">
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{{$environment->name}}</td>
                    </tr>
                    <tr>
                        <td>IP address</td>
                        <td>{{$environment->ip_address}}</td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td>
                            @if($environment->isRunning())
                                <i class="fa fa-check-circle" style="color:green"></i> Environment is Running
                            @else
                                <i class="fa fa-times-circle" style="color:red"></i> Environment is not Running
                            @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Planned Features
                        </button>
                    </h5>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item list-group-item-success">Ability to determine if its running or not and to start/stop it from running (stopping may be hard)</li>
                            <li class="list-group-item list-group-item-warning">Get info on server, apache/mysql/php version</li>
                            <li class="list-group-item list-group-item-warning">Ability to run updates (apt-get update)</li>
                            <li class="list-group-item list-group-item-success">As of right now I'm assigning ip address for the env by building Hyper-V virtual network interface
                                https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/user-guide/setup-nat-network, need to make that more intuitive (need to specifically build that automatically then
                                set the /etc/apache2/ports.conf file to listen on envip:port instead of just listening on all ips for the ports). This is a limitation of WSL and using a Hyper-V nat network is the
                                only way I have found around it. Making each environment apache only listen for the specific ip allows multiple of them to listen on the same port on the computer. Also bind-address
                                for [mysqld] in the /etc/mysql/my.cnf needs to be set to the env ip.
                            </li>
                            <li class="list-group-item list-group-item-warning">Add Ability to run additional scripts on the environment (apt commands, anything), can even add some helper tools at some point for this like installing php
                                libraries (can use apt-cache pkgnames | grep php{version}-* on env to get list of packages)
                            </li>
                            <li class="list-group-item list-group-item-success">I want to be able to on add assign 2-3 letters that are then converted to an icon. I then want to figure out how to make the .exe for the environment use the
                                icon generated (IE Basesite envrionment could have an icon on your computer with letters BS to make it easy to distinguish which env is which) -- Did some looking into this, in order
                                to assign an icon to the exe the only way i have found is to use Resource Hacker found here http://www.angusj.com/resourcehacker/ . This does have a command line interface so I think
                                that this should still be possible will just have to require that to be installed on the computer in order for it to work.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container -->
@endsection