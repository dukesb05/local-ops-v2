@extends('layouts.app')

@section('content')
<!-- Page Content -->
<div class="container">

	<!-- Page Heading -->
	<h1 class="my-4">Link Title Checker</h1>
	<form method="get" name="linkTitleChecker" class="form-horizontal">
		<div class="form-group">
			<input name="url" value="<?php echo $url; ?>" placeholder="Url to Check" class="form-control" />
		</div>
		<div class="form-group">
			<input type="submit" value="Check Url" class="btn btn-primary" />
		</div>
	</form>
	<?php if ($url) { ?>
		<?php if ($links_missing_title) { ?>
			<table class="table table-responsive table-striped">
				<tr>
					<th>Line Num</th>
					<th>Link Href</th>
					<th>Node Value</th>
					<th>Text Content</th>
				</tr>
				<?php foreach($links_missing_title as $link){ ?>
				<tr>
					<td><?php echo $link['line_num']+18; ?></td>
					<td><?php echo $link['href']; ?></td>
					<td><?php echo $link['nodeValue']; ?></td>
					<td><?php echo $link['textContent']; ?></td>
				</tr>
				<?php } ?>
			</table>
		<?php } else { ?>
			<p class="alert alert-success">All links on this url have titles!</p>
		<?php } ?>
	<?php } ?>
</div>
<!-- /.container -->
@endsection