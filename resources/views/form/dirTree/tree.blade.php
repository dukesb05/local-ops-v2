<a href="#{{$htmlId}}" class="list-group-item list-group-item-action dirTreeFolder{{$value == $directory['win'] ? ' active' : ''}}" data-toggle="collapse" data-directory='{{$directory['ubuntu']}}' data-subid='{{$htmlId}}' data-windowsdir='{{$directory['win']}}'>
	<i class="fa fa-caret-{{!empty($directory['subDirs']) ? 'down' : 'right'}}"></i>{{$directory['win']}}
	<span class="pull-right">
		<span class="btn btn-xs btn-block dirTreeSelectFolder">
			<span class="fa fa-{{$value == $directory['win'] ? 'minus' : 'check'}}-square dirTreeIcon" aria-hidden="true"></span>
		</span>
	</span>
</a>
@php
echo \App\DirTree::subDirs($directory, $htmlId);
@endphp