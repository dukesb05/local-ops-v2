<div class="card">
	<a href="#" data-toggle="collapse" data-target="#{{$id}}-directoryTree" aria-expanded="true" aria-controls="{{$id}}-directoryTree">
		<div class="card-header text-center dirTreeHeading">
			{{$value ? $value : 'Directory Tree'}}
		</div>
	</a>
	<div class="card-body collapse" id="{{$id}}-directoryTree">
		<div class="list-group list-group-root well">
			@foreach($directories as $directory)
			@include('form.dirTree.tree', ['directory'=>$directory, 'htmlId'=>($id != 'environment_root_dir' ? $id : '').\App\DirTree::folderHtmlId($directory['ubuntu']), 'value'=>$value])
			@endforeach
		</div>
		<input class="dirTreeValue" name="{{$name}}" type="hidden" value="{{$value}}" />
	</div>
</div>
