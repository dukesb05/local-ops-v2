<div class="card">
	<div class="card-header">
		SSH Key
	</div>
	<div class="card-body">
		<div class="well">
			@if(!empty($sshKey))
			<span style="width:98%; word-wrap:break-word; display:inline-block;">
				{{$sshKey}}
			</span>
			@else
			{{'gen key'}}
			@endif
		</div>
	</div>
</div>
