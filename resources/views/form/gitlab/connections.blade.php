<div class="card">
	<div class="card-header">
		@foreach($connections as $conn_name => $connection)
		<h3>{{ucwords($conn_name)}}</h3>

		@endforeach
	</div>
	<div class="card-body">
		<div class="well">
			@foreach($connections as $conn_name => $connection)
			<div class="gitlab-connection">
				<label for="connections[{{$conn_name}}][cache]" class="col-form-label">
				<input type="checkbox" name="connections[{{$conn_name}}][cache]" class="form-control"{{$connection['cache'] == true ? " checked" : ""}} />
				Enable Cache</label>
				<div class="clearfix"></div>
				<label for="connections[{{$conn_name}}][method]" class="col-form-label">Method</label>
				<select name="connections[{{$conn_name}}][method]" class="form-control">
					@foreach(['token', 'oauth', 'none'] as $method)
					<option value='{{$method}}'{{$method == $connection['method'] ? " selected='selected'" : ""}}>{{$method}}</option>
					@endforeach
				</select>
				<label for="connections[{{$conn_name}}][token]" class="col-form-label">Token</label>
				<input type="text" name="connections[{{$conn_name}}][token]" class="form-control" value="{{$connection['token']}}" />
			</div>
			@endforeach
		</div>
	</div>
</div>
