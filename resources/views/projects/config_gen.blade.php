@extends('layouts.app')

@section('content')
<!-- Page Content -->
<div class="container">
	<!-- Page Heading -->
	<h1 class="my-4">Project Config File Generator</h1>
	<div class="card">
		<div class="card-body">
			{{ Form::open([ 'id'=>'projectConfigGen', 'method'  => 'get', 'route' => [ 'projects.contents', $project_id, 'configFile' ] ]) }}
				{!! Form::label('project_id', 'Project', ['class' => 'control-label', 'required'=>true]) !!}
				{!! Form::select('project_id', $projects, $project_id, ['class' => 'form-control', 'required'=>true]) !!}
				{!! Form::label('file_type', 'File Type', ['class' => 'control-label', 'required'=>true]) !!}
				{!! Form::select('file_type', $file_types, $file_type, ['class' => 'form-control', 'required'=>true]) !!}
				{{ Form::submit('Generate', ['class' => 'btn btn-primary']) }}
			{{ Form::close() }}
			
			<?php echo $config_file; ?>
		</div>
	</div>
</div>
<!-- /.container -->
@endsection