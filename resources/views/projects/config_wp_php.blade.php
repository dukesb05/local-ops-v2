<ul class="nav nav-tabs" id="myTab" role="tablist">
	<?php $firstTab = true; ?>
	<?php foreach ($deployConfig['environments'] as $key => $env) { ?>
		<li class="nav-item">
			<a class="nav-link<?php echo $firstTab ? ' active' : ''; ?>" id="env-<?php echo $key; ?>-tab" data-toggle="tab" href="#env-<?php echo $key; ?>" role="tab" aria-controls="<?php echo $key; ?>" aria-selected="true"><?php echo $env['label']; ?></a>
		</li>
		<?php $firstTab = false; ?>
	<?php } ?>
</ul>
<div class="tab-content" id="myTabContent">
	<?php $firstTab = true; ?>
	<?php foreach ($deployConfig['environments'] as $key => $env) { ?>
		<div class="tab-pane fade<?php echo $firstTab ? ' show active' : ''; ?>" id="env-<?php echo $key; ?>" role="tabpanel" aria-labelledby="env-<?php echo $key; ?>-tab">
			<a class="btn btn-primary copyConfigFile"><i class="fa fa-copy"></i></a>
			<?php ob_start(); ?>
			<code class="config-code" style="display:block;white-space:pre-wrap;">
				define('DB_NAME','<?php echo $env['db']['name']; ?>');
				define('DB_USER','<?php echo $env['db']['user']; ?>');
				define('DB_PASSWORD','<?php echo $env['db']['pass']; ?>');
				define('DB_HOST','<?php echo $env['db']['host']; ?>');
				$table_prefix  = '<?php echo!empty($env['db']['prefix']) ? $env['db']['prefix'] : ''; ?>';

				define('WP_ENV','development');
				define('WP_HOME','https://<?php echo $project->mainDomain()->domain; ?>');
				define('WP_SITEURL','https://<?php echo $project->mainDomain()->domain; ?>');

				/**
				* This value needs to be defined when on multisite
				*/
				//define('DOMAIN_CURRENT_SITE','example.dev');


				define( 'FORCE_SSL_LOGIN', false );
				define( 'FORCE_SSL_ADMIN', false );
				define( 'DISALLOW_FILE_EDIT', true );
				define('WP_CACHE', false);
				define('WP_DEBUG', false);
				define('WP_POST_REVISIONS', 5);
				define('EMPTY_TRASH_DAYS', 7);
				define('WP_MEMORY_LIMIT', '128M');
				define('CONCATENATE_SCRIPTS', false);

				// generate salts here: https://api.wordpress.org/secret-key/1.1/salt/
				define('AUTH_KEY','<?php echo $env['wpconfig']['auth_key']; ?>');
				define('SECURE_AUTH_KEY','<?php echo $env['wpconfig']['secure_auth_key']; ?>');
				define('LOGGED_IN_KEY','<?php echo $env['wpconfig']['logged_in_key']; ?>');
				define('NONCE_KEY','<?php echo $env['wpconfig']['nonce_key']; ?>');
				define('AUTH_SALT','<?php echo $env['wpconfig']['auth_salt']; ?>');
				define('SECURE_AUTH_SALT','<?php echo $env['wpconfig']['secure_auth_salt']; ?>');
				define('LOGGED_IN_SALT','<?php echo $env['wpconfig']['logged_in_salt']; ?>');
				define('NONCE_SALT','<?php echo $env['wpconfig']['nonce_salt']; ?>');

				define('DB_CHARSET', 'utf8');
				define('DB_COLLATE', '');
			</code>
			<?php echo preg_replace('/\\n/', '', trim(str_replace("\t", "", ob_get_clean())), 1); ?>
		</div>
		<?php $firstTab = false; ?>
	<?php } ?>
</div>