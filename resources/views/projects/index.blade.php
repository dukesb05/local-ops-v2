@extends('layouts.app')

@section('content')
<!-- Page Content -->
<div class="container">

	<!-- Page Heading -->
	<h1 class="my-4">Projects
		<small><a class="btn btn-primary ajaxModal" href="{{ route("projects.create") }}">Add Project</a></small>
	</h1>
	@if (count($projects) > 0)
	<h4><em>Filter</em> [<a href="#" class="project_filter_all">show all</a>]</h4>
	<ul class="nav nav-pills">
		<?php foreach (range('A', 'Z') as $char) { ?>
		<li class="nav-item"><a href="#" class="project_filter nav-link" data-char="<?php echo $char; ?>"><?php echo $char; ?></a></li>
		<?php } ?>
	</ul>

	<table class="table table-striped table-bordered table-hover">
		<thead class="thead-dark">
			<tr>
				<th>Name</th>
				<th>Type</th>
				<th>Domain(s)</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($projects as $project)
			<tr class="project_row char_<?php echo strtoupper($project->name[0]); ?>">
				<td><a href="{{ $project->link() }}">{{ $project->name }}</a></td>
				<td>{{ $project->type->name }}</td>
				<td>
					@foreach($project->domains as $domain)
					<a href="{{$project->link('launch', ['domain'=>$domain->id])}}" target="_blank">{{$domain->domain}}</a> <br />
					@endforeach
				</td>
				<td>
					<a href="{{ $project->link() }}" title="View" class="btn btn-sm btn-secondary"><i class="fa fa-eye"></i> View</a>
					{{ Form::open([ 'method'  => 'delete', 'route' => [ 'projects.destroy', $project->id ], 'style'=>'float:right;' ]) }}
					{{ Form::submit('Delete', ['class' => 'btn btn-sm btn-danger']) }}
					{{ Form::close() }}
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	@else
	<p class="alert alert-info">There are no Projects set up yet.</p>
	@endif

	<div class="accordion" id="accordionExample">
		<div class="card">
			<div class="card-header" id="headingTwo">
				<h5 class="mb-0">
					<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
						Planned Features
					</button>
				</h5>
			</div>
			<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
				<div class="card-body">
					<ul class="list-group">
						<li class="list-group-item">I want to incorporate our deploy-config into this here so that we dont have to
							request to get db information for environments. With it connected to gitlab and our deploy config
							following gitlabs group/project-name pattern we should be able to determine from project selected the
							deploy-config for it. I'm thinking that on initial add you have 2 options, whether or not to sync db
							down locally (or just use a deploy-config env) and which env to use from the deploy config (if syncing
							to sync from if not then to use as config creds). This info would be stored with the domain so that the
							domain determines the db to use (whether locally or remotely). This creates a scenario where you could
							have 2 diff domains for a project locally, each pulling from a different database.
						</li>
						<li class="list-group-item">Environments would also be controlled on a domain level with a default one being
							applied on project type. This allows the same project the ability to be loaded in multiple environments.
						</li>
						<li class="list-group-item">Each project should be launched from this system by clicking on the url. The
							urls take you to a launch route on this system that will auto set the config for the project for you.
							That way you can switch between environments much easier.
						</li>
					</ul>
				</div>
			</div>
			<div class="card-header" id="headingThree">
				<h5 class="mb-0">
					<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
						Dreaming Features
					</button>
				</h5>
			</div>
			<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
				<div class="card-body">
					<ul class="list-group">
						<li class="list-group-item">Could make a chrome extension that interacts with this system. Could make
							updating composer, switching envs, updating assets (laravel) very easy to do on the site.
						</li>
						<li class="list-group-item">We have the ability to run pretty much any windows command from this system.
							Would be cool to be able to directly open the navicat connection to an environment and open the netbeans
							project for a project. This is however all heavily dependant on those programs offering that
							functionality.
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.container -->
@endsection
@section('page-js-script')
<script>
	$(function(){
		$('.project_filter').click(function(e){
			e.preventDefault();
			var char = $(this).data('char');
			$('.project_row').fadeOut('fast');
			$('.project_row.char_'+char).fadeIn('fast');
			$('.nav-link.active').removeClass('active');
			$(this).addClass('active');
		});
		$('.project_filter_all').click(function(e){
			e.preventDefault();
			$('.project_row').fadeIn('fast');
			$('.nav-link.active').removeClass('active');
		});
	});
</script>
@endsection