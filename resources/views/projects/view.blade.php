@extends('layouts.app')
@section('content')
<!-- Page Content -->
<div class="container">

	<!-- Page Heading -->
	<h1 class="my-4">
		<a href="{{route('projects.index')}}" class="btn btn-sm btn-secondary"><i class="fa fa-arrow-left"></i></a> Projects -
		<small><a href="{{$project->link('launch', ['domain'=>$project->mainDomain()->id])}}" target="_blank"><?php echo $project->name; ?></a></small>
	</h1>

	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-3 pull-left">
					<ul class="nav flex-column nav-pills">
						<li class="nav-item"><a href="" class="nav-link active">Home</a></li>
						<li class="nav-item"><a href="{{$project->link('contents', ['content'=>'configFile'])}}" class="nav-link projectContentUpdate">Config File</a></li>
						<li class="nav-item"><a href="{{$project->link('contents', ['content'=>'htaccessMedia'])}}" class="nav-link projectContentUpdate">.htaccess Media</a></li>
						<li class="nav-item"><a href="{{$project->link('contents', ['content'=>'domains'])}}" class="nav-link projectContentUpdate">Domains</a></li>
					</ul>
				</div>
				<div class="col-9 pull-left project-content">
					<div class="row">
						<div class="col-3 pull-left text-center img-thumbnail">
							<a href="{{$project->link('edit')}}" title="Project Edit" class="btn btn-link ajaxModal">
								<i class="fa fa-edit fa-5x"></i>
								<h3>Edit</h3>
							</a>
						</div>
						<div class="col-3 pull-left text-center img-thumbnail">
							<a href="<?php echo $project->repoWebUrl(); ?>" title="Gitlab" class="btn btn-link" target="_blank">
								<i class="fa fa-gitlab fa-5x"></i>
								<h3>Gitlab</h3>
							</a>
						</div>
						<div class="col-3 pull-left text-center img-thumbnail">
							<a href="<?php echo $project->opsUrl(); ?>" title="OPS" class="btn btn-link" target="_blank">
								<i class="fa fa-wrench fa-5x"></i>
								<h3>OPS</h3>
							</a>
						</div>
						<?php if ($project->hasComposer()) { ?>
							<div class="col-3 pull-left text-center img-thumbnail">
								<a href="{{$project->link('actions', ['action'=>'updateComposer'])}}" title="Update Composer" class="btn btn-link ajaxAction">
									<i class="fa fa-terminal fa-5x"></i>
									<h3>Composer</h3>
								</a>
							</div>
						<?php } ?>
					</div>
					<div class="row">
						<div class="col-3 pull-left text-center img-thumbnail">
							<a href="{{$project->link('actions', ['action'=>'openTerminal'])}}" title="Project Terminal" class="btn btn-link ajaxAction">
								<i class="fa fa-terminal fa-5x"></i>
								<h3>Terminal</h3>
							</a>
						</div>
						<div class="col-3 pull-left text-center img-thumbnail">
							<a href="{{$project->link('actions', ['action'=>'openFolder'])}}" title="Project Folder" class="btn btn-link ajaxAction">
								<i class="fa fa-folder fa-5x"></i>
								<h3>Folder</h3>
							</a>
						</div>
						<div class="col-3 pull-left text-center img-thumbnail">
							<a href="{{$project->link('actions', ['action'=>'openNetbeans'])}}" title="Project Netbeans" class="btn btn-link ajaxAction">
								<i class="fa fa-cube fa-5x"></i>
								<h3>Netbeans</h3>
							</a>
						</div>
						<div class="col-3 pull-left text-center img-thumbnail">
							<a href="{{$project->link('actions', ['action'=>'apacheLogs'])}}" title="Apache Logs" class="btn btn-link ajaxAction">
								<i class="fa fa-server fa-5x"></i>
								<h3>Apache Logs</h3>
							</a>
						</div>
					</div>
					<div class="row">
						<div class="col-3 pull-left text-center img-thumbnail">
							<a href="{{$project->link('actions', ['action'=>'clearApacheLogs'])}}" title="Apache Logs" class="btn btn-link ajaxAction">
								<i class="fa fa-trash fa-5x"></i>
								<h3>Clear Apache</h3>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.container -->
@endsection
@section('page-js-script')
<script>
    $(function () {
        $('.nav-link').click(function (e) {
            $('.nav-link.active').removeClass('active');
            $(this).addClass('active');
        });
        $('.ajaxAction').click(function (e) {
            e.preventDefault();
            $.post($(this).attr('href'), {}, function (response) {
                var msg = '<p class="alert alert-';
                if (response.status == 'success') {
                    msg += 'success';
                } else {
                    msg += 'danger';
                }
                msg += '">' + response.msg + '</p>';
                var modal = bootbox.alert(msg);
                modal.on("shown.bs.modal", function () {
                    setTimeout(function () {
                        modal.modal('hide');
                    }, 1000);
                });
            }, 'json');
        });
        $(document).on('click', '.projectContentUpdate', function (e) {
            e.preventDefault();
            var link = $(this);
            var replaceDiv = $('.project-content');
            $.get(link.attr('href'), {}, function (response) {
                replaceDiv.html(response.html);
            });
        });
        $(document).on('submit', '#projectConfigGen', function (e) {
            e.preventDefault();
            var link = $(this);
            var replaceDiv = $('.project-content');
            var keys = [
                "AUTH_KEY",
                "SECURE_AUTH_KEY",
                "LOGGED_IN_KEY",
                "NONCE_KEY",
                "AUTH_SALT",
                "SECURE_AUTH_SALT",
                "LOGGED_IN_SALT",
                "NONCE_SALT"
            ];

            var getRandom = function (min, max) {
                var rval = 0;
                var range = max - min;

                var bits_needed = Math.ceil(Math.log2(range));
                if (bits_needed > 53) {
                    throw new Exception("We cannot generate numbers larger than 53 bits.");
                }
                var bytes_needed = Math.ceil(bits_needed / 8);
                var mask = Math.pow(2, bits_needed) - 1;
                // 7776 -> (2^13 = 8192) -1 == 8191 or 0x00001111 11111111

                // Create byte array and fill with N random numbers
                var byteArray = new Uint8Array(bytes_needed);
                window.crypto.getRandomValues(byteArray);

                var p = (bytes_needed - 1) * 8;
                for (var i = 0; i < bytes_needed; i++) {
                    rval += byteArray[i] * Math.pow(2, p);
                    p -= 8;
                }

                // Use & to apply the mask and reduce the number of recursive lookups
                rval = rval & mask;

                if (rval >= range) {
                    // Integer out of acceptable range
                    return getRandom(min, max);
                }
                // Return an integer that falls within the range
                return min + rval;
            };

            var getRandomChar = function () {

                var minChar = 33; // !
                var maxChar = 126; // ~
                var char = String.fromCharCode(getRandom(minChar, maxChar));
                if (["'", "\"", "\\", "<"].some(function (badChar) {
                    return char === badChar
                })) {
                    return getRandomChar();
                }

                return char;
            };

            var generateSalt = function () {
                return Array.apply(null, Array(64)).map(getRandomChar).join("");
            };

            var generateEnvLine = function (mode, key) {
                var salt = generateSalt();
                switch (mode) {
                    case "php":
                        return "define('" + key.toUpperCase() + "','" + salt + "');";
                    default:
                        return key.toUpperCase() + "='" + salt + "'";
                }
            };

            generateFile = function (mode, keys) {
                return keys.map(generateEnvLine.bind(null, mode)).join("\n");
            };


            var createSection = function (type, keys) {
                var text = generateFile(type, keys);
                return text;
            };
            var salts = createSection("env", keys);
            var php_salts = createSection("php", keys);
            $.get(link.attr('action'), link.serialize(), function (response) {
                replaceDiv.html(response.html);
                $('.config-code').each(function (i, v) {
                    if ($(v).html().indexOf("[WP_ENV_SALTS]") !== -1) {
                        $(v).html($(v).html().replace("[WP_ENV_SALTS]", salts));
                    }
                    if ($(v).html().indexOf("[WP_PHP_SALTS]") !== -1) {
                        $(v).html($(v).html().replace("[WP_PHP_SALTS]", php_salts));
                    }
                });
            });
        });
    });
</script>
@endsection