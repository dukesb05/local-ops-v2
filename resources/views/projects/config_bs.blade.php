<ul class="nav nav-tabs" id="myTab" role="tablist">
	<?php $firstTab = true; ?>
	<?php foreach ($deployConfig['environments'] as $key => $env) { ?>
		<li class="nav-item">
			<a class="nav-link<?php echo $firstTab ? ' active' : ''; ?>" id="env-<?php echo $key; ?>-tab" data-toggle="tab" href="#env-<?php echo $key; ?>" role="tab" aria-controls="<?php echo $key; ?>" aria-selected="true"><?php echo $env['label']; ?></a>
		</li>
		<?php $firstTab = false; ?>
	<?php } ?>
</ul>
<div class="tab-content" id="myTabContent">
	<?php $firstTab = true; ?>
	<?php foreach ($deployConfig['environments'] as $key => $env) { ?>
		<div class="tab-pane fade<?php echo $firstTab ? ' show active' : ''; ?>" id="env-<?php echo $key; ?>" role="tabpanel" aria-labelledby="env-<?php echo $key; ?>-tab">
			<a class="btn btn-primary copyConfigFile"><i class="fa fa-copy"></i></a>
			<?php ob_start(); ?>
			<code class="config-code" style="display:block;white-space:pre-wrap;">
				define('DB_NAME', '<?php echo $env['db']['name']; ?>');
				define('DB_UNAME', '<?php echo $env['db']['user']; ?>');
				define('DB_PWORD', '<?php echo $env['db']['pass']; ?>');
				define('DB_HOST', '<?php echo $env['db']['host']; ?>');

				define('ENV', 'local');
				/*

				define('SMTP_HOST',		'smtp.mailtrap.io');
				define('SMTP_PORT',		'2525');
				define('SMTP_EMAIL',	'info@coastalstylemag.com');
				define('SMTP_UNAME',	'');
				define('SMTP_PWORD',	'');
				define('SMTP_SECURE', false);

				*/
			</code>
			<?php echo preg_replace('/\\n/', '', trim(str_replace("\t", "", ob_get_clean())), 1); ?>
		</div>
		<?php $firstTab = false; ?>
	<?php } ?>
</div>