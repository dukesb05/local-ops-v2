@extends('layouts.app')

@section('content')
<!-- Page Content -->
<div class="container">

	<!-- Page Heading -->
	<h1 class="my-4">Projects - 
		<small>Setup {{$project->name}}</small>
	</h1>
	<div class="well col-12">
		<a href="{{ $project->link('setup') }}" data-statuslink='{{ $project->link('setupStatus') }}' class="btn btn-primary environmentSetup col-12">Setup Project</a>
	</div>
	<ul class="list-group list-group-root well">
		@foreach($project->setupClass()->getSteps() as $step)
		<li class="list-group-item">{{ucwords(str_replace('_', ' ', $step))}}</li>
		@endforeach
	</ul>
</div>
<!-- /.container -->
@endsection