{!! Form::model($project_type, ['route' => ['projects.types.update', $project_type->id], 'class'=>'needs-validation']) !!}
<h3>Project Type Info</h3>
<div class="form-group">
	{!! Form::label('name', 'Name', ['class' => 'control-label', 'required'=>true]) !!}
	{!! Form::text('name', null, ['class' => 'form-control', 'required'=>true]) !!}
	<div class="invalid-feedback">
		Please enter a Name.
	</div>
</div>
<div class="form-group">
	{!! Form::label('default_env', 'Default Environment', ['class' => 'control-label', 'required'=>true]) !!}
	{!! Form::select('default_env', $envs, null, ['class' => 'form-control', 'required'=>true]) !!}
	<div class="invalid-feedback">
		Please select a Default Environment.
	</div>
</div>
<div class="form-group">
	{!! Form::label('requires_local_db', 'Requires Local DB', ['class' => 'control-label']) !!}
	{!! Form::checkbox('requires_local_db', 1, $project_type->requires_local_db ? true : false) !!}
</div>
<div class="form-group">
	{!! Form::label('composer_cmd', 'Composer Command', ['class' => 'control-label', 'required'=>true]) !!}
	{!! Form::text('composer_cmd', null, ['class' => 'form-control', 'required'=>true]) !!}
	<div class="invalid-feedback">
		Please enter a Composer Command.
	</div>
</div>
<div class="form-group">
	{!! Form::label('deploy_config_types', 'Deploy Config Types', ['class' => 'control-label']) !!}
	{!! Form::checkboxes('deploy_config_types[]', $deployConfigTypes, $selectedDeployConfigTypes) !!}
</div>
<div class="form-group">
	{!! Form::submit('Save Project Type',['class' => 'btn btn-primary ajaxForm']) !!}
</div>
{!! Form::close() !!}