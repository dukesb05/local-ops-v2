@extends('layouts.app')

@section('content')
<!-- Page Content -->
<div class="container">

	<!-- Page Heading -->
	<h1 class="my-4">Project Types
		<small><a class="btn btn-primary ajaxModal" href="{{ route("projects.types.create") }}">Add Project Type</a></small>
	</h1>
	@if (count($project_types) > 0)
	<div class="row">
		@foreach ($project_types as $project_type)
		<div class="col-lg-3 col-md-4 col-sm-6 pull-left text-center img-thumbnail">
			<a href="{{ $project_type->link() }}" class="ajaxModal">
				<i class="fa fa-<?php echo strtolower($project_type->name); ?> fa-5x"></i>
				<h3>{{ $project_type->name }}</h3>
			</a>
		</div>
		@endforeach
	</div>
	<!-- /.row -->
	@else
	<p class="alert alert-info">There are no Project Types set up yet.</p>
	@endif
</div>
<!-- /.container -->
@endsection