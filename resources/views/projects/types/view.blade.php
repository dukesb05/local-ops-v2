@extends('layouts.app')

@section('content')
<!-- Page Content -->
<div class="container">

	<!-- Page Heading -->
	<h1 class="my-4">Project Types - 
		<small>{{$type->name}}</small>
		<p>
		<h3>Planned Features</h3>
		<ul class="list-group">
			<li class="list-group-item">Show information here about the type with it being editable</li>
			<li class="list-group-item">Define default config file for project type (may want to define multiple defaults to be able to set for project type, thinking BS here mostly with some having env.php in root and some having app/config/config.env.php). Then on project creation you could choose from one of these defaults for the projects config file or could manually enter one (for rare cases)</li>
			<li class="list-group-item">I want to define the config template here using standard variables like @{{DB_HOST}} that way can build the different config files there for each site. Can have a way to override this also per project. Also thinking of having a way per project to add config vars so for example if you have a site with authnet and want to put your local creds in for your local site you could add those config vars directly in the project and wouldnt have to override the entire config template (these would just be appended to end of config)</li>
			<li class="list-group-item">There will be a large pool of tools on the site that are defined as project/environment/server types as to where they work. Some of them will be needed to properly setup a set up a specific project type (ie magento needing the composer redeploy command run so symlinks work properly). Going to have a way here to assign tools that run on setup for projects so that magento sites can have that composer script ran (and anything else we find we need to run on a per project basis).</li>
			<li class="list-group-item">Same as above except there will also be tools that display on the project view page to run after it is setup. An example would be resyncing the database and updating composer.</li>
		</ul>
		</p>
	</h1>
</div>
<!-- /.container -->
@endsection