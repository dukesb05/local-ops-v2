{!! Form::model($configfile, ['route' => ['projects.configfiles.update', $configfile->id], 'class'=>'needs-validation']) !!}
<h3>Project Config File Info</h3>
<div class="form-group">
	{!! Form::label('name', 'Name', ['class' => 'control-label', 'required'=>true]) !!}
	{!! Form::text('name', null, ['class' => 'form-control', 'required'=>true]) !!}
	<div class="invalid-feedback">
		Please enter a Name.
	</div>
</div>
<div class="form-group">
	{!! Form::label('key', 'Key', ['class' => 'control-label', 'required'=>true]) !!}
	{!! Form::text('key', null, ['class' => 'form-control', 'required'=>true, 'disabled' => true]) !!}
	<div class="invalid-feedback">
		Please enter a Key.
	</div>
</div>
<div class="form-group">
	{!! Form::label('deploy_config_build_tasks', 'Deploy Config Build Tasks', ['class' => 'control-label']) !!}
	{!! Form::checkboxes('deploy_config_build_tasks[]', $deployConfigBuildTasks, $selectedDeployConfigBuildTasks) !!}
</div>
<div class="form-group">
	{!! Form::submit('Save Project Config File',['class' => 'btn btn-primary ajaxForm']) !!}
</div>
{!! Form::close() !!}