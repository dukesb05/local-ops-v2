@extends('layouts.app')

@section('content')
<!-- Page Content -->
<div class="container">

	<!-- Page Heading -->
	<h1 class="my-4">Config Files
		<small><a class="btn btn-primary ajaxModal" href="{{ route("projects.configfiles.create") }}">Add Project Config File</a></small>
	</h1>
	@if (count($configFiles) > 0)
	<table class="table table-striped table-bordered table-hover">
		<thead class="thead-dark">
			<tr>
				<th>Name</th>
				<th>Key</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($configFiles as $configFile)
			<tr class="project_row char_<?php echo strtoupper($configFile->name[0]); ?>">
				<td><a href="{{ $configFile->link() }}" class="ajaxModal">{{ $configFile->name }}</a></td>
				<td>{{ $configFile->key }}</td>
				<td>
					<a href="{{ $configFile->link() }}" title="Edit" class="btn btn-sm btn-secondary ajaxModal"><i class="fa fa-edit"></i> Edit</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	@else
	<p class="alert alert-info">There are no Project Config Files set up yet.</p>
	@endif
</div>
<!-- /.container -->
@endsection