<ul class="nav nav-tabs" id="myTab" role="tablist">
	<?php $firstTab = true; ?>
	<?php foreach ($deployConfig['environments'] as $key => $env) { ?>
		<li class="nav-item">
			<a class="nav-link<?php echo $firstTab ? ' active' : ''; ?>" id="env-<?php echo $key; ?>-tab" data-toggle="tab" href="#env-<?php echo $key; ?>" role="tab" aria-controls="<?php echo $key; ?>" aria-selected="true"><?php echo $env['label']; ?></a>
		</li>
		<?php $firstTab = false; ?>
	<?php } ?>
</ul>
<div class="tab-content" id="myTabContent">
	<?php $firstTab = true; ?>
	<?php foreach ($deployConfig['environments'] as $key => $env) { ?>
		<div class="tab-pane fade<?php echo $firstTab ? ' show active' : ''; ?>" id="env-<?php echo $key; ?>" role="tabpanel" aria-labelledby="env-<?php echo $key; ?>-tab">
			<a class="btn btn-primary copyConfigFile"><i class="fa fa-copy"></i></a>
			<?php ob_start(); ?>
			<code class="config-code" style="display:block;white-space:pre-wrap;">			
				DB_NAME=<?php echo $env['db']['name'] . "\n"; ?>
				DB_USER=<?php echo $env['db']['user'] . "\n"; ?>
				DB_PASSWORD=<?php echo $env['db']['pass'] . "\n"; ?>
				DB_HOST=<?php echo $env['db']['host'] . "\n"; ?>
				DB_PREFIX=d3_wp_

				WP_ENV=development
				WP_HOME=https://<?php echo $project->mainDomain()->domain . "\n"; ?>
				WP_SITEURL=${WP_HOME}/wp

				# Generate your keys here: https://roots.io/salts.html
				AUTH_KEY='<?php echo $env['wpconfig']['auth_key']; ?>'
				SECURE_AUTH_KEY='<?php echo $env['wpconfig']['secure_auth_key']; ?>'
				LOGGED_IN_KEY='<?php echo $env['wpconfig']['logged_in_key']; ?>'
				NONCE_KEY='<?php echo $env['wpconfig']['nonce_key']; ?>'
				AUTH_SALT='<?php echo $env['wpconfig']['auth_salt']; ?>'
				SECURE_AUTH_SALT='<?php echo $env['wpconfig']['secure_auth_salt']; ?>'
				LOGGED_IN_SALT='<?php echo $env['wpconfig']['logged_in_salt']; ?>'
				NONCE_SALT='<?php echo $env['wpconfig']['nonce_salt']; ?>'			
			</code>
			<?php echo preg_replace('/\\n/', '', trim(str_replace("\t", "", ob_get_clean())), 1); ?>
		</div>
		<?php $firstTab = false; ?>
	<?php } ?>
</div>