<?php foreach ($deployConfig['environments'] as $key => $environment) { ?>
	<div class="card">
		<div class="card-header text-center">
			<h5><?php echo $environment['label']; ?></h5>
		</div>
		<?php $url = str_replace(['{{ git_project }}', '{{ git_group }}'], [$deployConfig['git_project'], $deployConfig['git_group']], $environment['server']['home_url']); ?>
		<a class="btn btn-primary copyConfigFile" style="top:55px;right:3px;"><i class="fa fa-copy"></i></a>
		<?php ob_start(); ?>
		<code style="display:block;white-space:pre-wrap;">
			&lt;IfModule mod_rewrite.c&gt;
			RewriteEngine on
			RewriteCond %{REQUEST_FILENAME} !-d
			RewriteCond %{REQUEST_FILENAME} !-f
			RewriteRule ([^.]+\.(jpe?g|gif|bmp|png))$ <?php echo $url; ?>/$1 [R=301,L,NC]
			&lt;/IfModule&gt;
		</code>
		<?php echo preg_replace('/\\n/', '', trim(str_replace("\t", "", ob_get_clean())), 1); ?>
	</div>
<?php } ?>