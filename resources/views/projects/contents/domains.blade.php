<table class="table table-responsive table-bordered table-striped">
	<tr>
		<th>Domain</th>
		<th>Environment</th>
		<th>Main</th>
		<th>SSL</th>
	</tr>
	<?php foreach($project->domains as $domain){ ?>
	<tr>
		<td><?php echo $domain->domain; ?></td>
		<td><a href="<?php echo $domain->environment()->link(); ?>"><?php echo $domain->environment()->name; ?></a></td>
		<td><?php echo $domain->main ? 'Yes' : 'No'; ?></td>
		<td><?php echo $domain->ssl ? 'Yes' : 'No'; ?></td>
	</tr>
	<?php } ?>
</table>