{!! Form::model($project, ['route' => ['projects.update', $project->id], 'class'=>'needs-validation']) !!}
<h3>Project Info</h3>
<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane active container" id="gitlab-info">
        {!! Form::hidden('repo_url', null, ['id'=>'repo_url']) !!}
        <div class="form-group">
            {!! Form::label('repo_id', 'Gitlab Project', ['class' => 'control-label', 'required'=>true]) !!}
            <div class="col-md-12">
                {!! Form::select('repo_id', [], null, ['class' => 'form-control', 'required'=>true]) !!}
            </div>
        </div>
    </div>
    <div class="tab-pane container" id="custom-info">
        <div class="form-group">
            {!! Form::label('type', 'Type', ['class' => 'control-label', 'required'=>true]) !!}
            {!! Form::select('type_id', $types, null, ['class' => 'form-control', 'required'=>true]) !!}
            <div class="invalid-feedback">
                Please select a Type.
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('name', 'Name', ['class' => 'control-label', 'required'=>true]) !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'required'=>true]) !!}
            <div class="invalid-feedback">
                Please enter a Name.
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('folder', 'Folder', ['class' => 'control-label', 'required'=>true]) !!}
            {!! Form::text('folder', null, ['class' => 'form-control', 'required'=>true]) !!}
            <div class="invalid-feedback">
                Please enter a Folder.
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('domain', 'Domain', ['class' => 'control-label', 'required'=>true]) !!}
            {!! Form::text('domain', null, ['class' => 'form-control', 'required'=>true]) !!}
            <div class="invalid-feedback">
                Please enter a Domain.
            </div>
        </div>
	<div class="form-group">
            {!! Form::label('domain_db_location', '', ['class' => 'control-label', 'required'=>true]) !!}
            {!! Form::select('domain_db_location', ['remote'=>'Remote', 'local'=>'Local'], null, ['class' => 'form-control', 'required'=>true]) !!}
            <div class="invalid-feedback">
                Please select a Domain Db Location.
            </div>
        </div>
	<div class="form-group">
            {!! Form::label('domain_local_db_name', '', ['class' => 'control-label']) !!}
            {!! Form::text('domain_local_db_name', null, ['class' => 'form-control']) !!}
            <div class="invalid-feedback">
                Please enter a Domain Local Db Name.
            </div>
        </div>
	<div class="form-group">
            {!! Form::label('domain_db_source', '', ['class' => 'control-label', 'required'=>true]) !!}
            {!! Form::select('domain_db_source', [], null, ['class' => 'form-control', 'required'=>true]) !!}
            <div class="invalid-feedback">
                Please select a Domain Db Source.
            </div>
        </div>
        <div class="form-group">
            <h3>Remote Db Info</h3>
            {!! Form::label('remote_db_host', 'Host') !!}
            {!! Form::text('remote_db_host', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('remote_db_user', 'User') !!}
            {!! Form::text('remote_db_user', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('remote_db_pass', 'Password') !!}
            {!! Form::text('remote_db_pass', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('remote_db_name', 'Name') !!}
            {!! Form::text('remote_db_name', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('remote_db_prefix', 'Prefix') !!}
            {!! Form::text('remote_db_prefix', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::submit('Save Project',['class' => 'btn btn-primary ajaxForm']) !!}
</div>
{!! Form::close() !!}