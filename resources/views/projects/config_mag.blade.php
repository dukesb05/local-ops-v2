<ul class="nav nav-tabs" id="myTab" role="tablist">
	<?php $firstTab = true; ?>
	<?php foreach ($deployConfig['environments'] as $key => $env) { ?>
		<li class="nav-item">
			<a class="nav-link<?php echo $firstTab ? ' active' : ''; ?>" id="env-<?php echo $key; ?>-tab" data-toggle="tab" href="#env-<?php echo $key; ?>" role="tab" aria-controls="<?php echo $key; ?>" aria-selected="true"><?php echo $env['label']; ?></a>
		</li>
		<?php $firstTab = false; ?>
	<?php } ?>
</ul>
<?php $prefix		 = !empty($deployConfig['environments']['production']['db']['prefix']) ? $deployConfig['environments']['production']['db']['prefix'] : ''; ?>
<?php $cryptKey	 = !empty($deployConfig['environments']['production']['mageconfig']['crypt_key']) ? $deployConfig['environments']['production']['mageconfig']['crypt_key'] : '25362690901038ea1e9113c70a9f6bc4'; ?>
<div class="tab-content" id="myTabContent">
	<div class="tab-pane fade show active" id="env-local" role="tabpanel" aria-labelledby="env-local-tab">
		<a class="btn btn-primary copyConfigFile"><i class="fa fa-copy"></i></a>
		<?php ob_start(); ?>
		<code style="display:block;white-space:pre-wrap;">			
			UPDATE <?php echo $prefix; ?>core_config_data SET value = 'https://<?php echo $project->mainDomain()->domain; ?>/' WHERE path IN ('web/unsecure/base_url','web/secure/base_url');
			SET FOREIGN_KEY_CHECKS=0;
			UPDATE <?php echo $prefix; ?>core_store SET store_id = 0 WHERE code='admin';
			UPDATE <?php echo $prefix; ?>core_store_group SET group_id = 0 WHERE name='Default';
			UPDATE <?php echo $prefix; ?>core_website SET website_id = 0 WHERE code='admin';
			UPDATE <?php echo $prefix; ?>customer_group SET customer_group_id = 0 WHERE customer_group_code='NOT LOGGED IN';
			SET FOREIGN_KEY_CHECKS=1;
			REPLACE INTO `<?php echo $prefix; ?>core_config_data`(`path`,`value`) VALUES ('dev/template/allow_symlink', 1);
		</code>
		<?php echo preg_replace('/\\n/', '', trim(str_replace("\t", "", ob_get_clean())), 1); ?>
		<div>
		<a class="btn btn-primary copyConfigFile" style="top:320px;"><i class="fa fa-copy"></i></a>
		<?php ob_start(); ?>
		<code class="config-code" style="display:block;white-space:pre-wrap;">
			<xmp>
				<?xml version="1.0"?>
				<!--
				/**
				 * Magento
				 *
				 * NOTICE OF LICENSE
				 *
				 * This source file is subject to the Academic Free License (AFL 3.0)
				 * that is bundled with this package in the file LICENSE_AFL.txt.
				 * It is also available through the world-wide-web at this URL:
				 * http://opensource.org/licenses/afl-3.0.php
				 * If you did not receive a copy of the license and are unable to
				 * obtain it through the world-wide-web, please send an email
				 * to license@magento.com so we can send you a copy immediately.
				 *
				 * DISCLAIMER
				 *
				 * Do not edit or add to this file if you wish to upgrade Magento to newer
				 * versions in the future. If you wish to customize Magento for your
				 * needs please refer to http://www.magento.com for more information.
				 *
				 * @category    Mage
				 * @package     Mage_Core
				 * @copyright   Copyright (c) 2006-2018 Magento, Inc. (http://www.magento.com)
				 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
				 */
				-->
				<config>
					<global>
						<install>
							<date><![CDATA[Fri, 22 Jun 2018 19:05:16 +0000]]></date>
						</install>
						<crypt>
							<key><![CDATA[<?php echo $cryptKey; ?>]]></key>
						</crypt>
						<disable_local_modules>false</disable_local_modules>
						<resources>
							<db>
								<table_prefix><![CDATA[<?php echo $prefix; ?>]]></table_prefix>
							</db>
							<default_setup>
								<connection>
									<host><![CDATA[127.0.0.1]]></host>
									<username><![CDATA[root]]></username>
									<password><![CDATA[root]]></password>
									<dbname><![CDATA[<?php echo str_replace('.test', '', $project->mainDomain()->domain); ?>]]></dbname>
									<initStatements><![CDATA[SET NAMES utf8]]></initStatements>
									<model><![CDATA[mysql4]]></model>
									<type><![CDATA[pdo_mysql]]></type>
									<pdoType><![CDATA[]]></pdoType>
									<active>1</active>
								</connection>
							</default_setup>
						</resources>
						<session_save><![CDATA[files]]></session_save>
					</global>
					<admin>
						<routers>
							<adminhtml>
								<args>
									<frontName><![CDATA[d3panel]]></frontName>
								</args>
							</adminhtml>
						</routers>
					</admin>
				</config>
			</xmp>
		</code>
		<?php echo preg_replace('/\\n/', '', trim(str_replace("\t", "", ob_get_clean())), 1); ?>
		</div>
	</div>
</div>