{!! Form::model($project, ['route' => ['projects.update', $project->id], 'class'=>'needs-validation']) !!}
<h3>Project Info</h3>
<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane active container" id="gitlab-info">
        {!! Form::hidden('repo_url', (!empty($gitlabProject['ssh_url_to_repo']) ? $gitlabProject['ssh_url_to_repo'] : ''), ['id'=>'repo_url']) !!}
        <div class="form-group">
            {!! Form::label('repo_id', 'Gitlab Project', ['class' => 'control-label', 'required'=>true]) !!}
            <div class="col-md-12">
                {!! Form::select('repo_id', $reposDefault, (!empty($gitlabProject['id']) ? $gitlabProject['id'] : ''), ['class' => 'form-control', 'required'=>true]) !!}
            </div>
        </div>
        <script>
            $('#repo_id').parents('.bootbox').removeAttr('tabindex'); // Add this line
            $('#repo_id').select2({
                placeholder: 'Select an item',
                minimumInputLength: 3,
                ajax: {
                    url: '/projects/gitlabSearch',
                    dataType: 'json'
                },
                width: '100%'
            });
            $('#repo_id').on('select2:select', function (e) {
                var data = e.params.data;
				if(data.local_id){
					window.location.href = '/projects/'+data.local_id;
				}
                getDbSourcesOptions(data.path_with_namespace, $('#domain_db_source'));
                $('#name').val(data.p_name);
                $('#folder').val(data.folder);
                $('#domain').val(data.domain);
                $('#domain_local_db_name').val(data.local_db_name);
                $('#repo_url').val(data.ssh_url_to_repo);
            });
            function getDbSourcesOptions(repo_path, ele) {
                $.get('/projects/create', {'projectAeAction': 'dbsourcesoptions', 'repo_path': repo_path}, function (response) {
                    ele.html(response);
                });
            }
            $('#domain_db_location').change(function (e) {
                if ($(this).val() == 'local') {
                    $('#domain_local_db_name').parent('.form-group').show();
                } else {
                    $('#domain_local_db_name').parent('.form-group').hide();
                }
            });
        </script>
    </div>
    <div class="tab-pane active container" id="custom-info">
        <div class="form-group">
            {!! Form::label('type', 'Type', ['class' => 'control-label', 'required'=>true]) !!}
            {!! Form::select('type_id', $types, $selectedType, ['class' => 'form-control', 'required'=>true]) !!}
            <div class="invalid-feedback">
                Please select a Type.
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('name', 'Name', ['class' => 'control-label', 'required'=>true]) !!}
            {!! Form::text('name', (!empty($gitlabProject['p_name']) ? $gitlabProject['p_name'] : ''), ['class' => 'form-control', 'required'=>true]) !!}
            <div class="invalid-feedback">
                Please enter a Name.
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('folder', 'Folder', ['class' => 'control-label', 'required'=>true]) !!}
            {!! Form::text('folder', (!empty($gitlabProject['folder']) ? $gitlabProject['folder'] : ''), ['class' => 'form-control', 'required'=>true]) !!}
            <div class="invalid-feedback">
                Please enter a Folder.
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('domain', 'Domain', ['class' => 'control-label', 'required'=>true]) !!}
            {!! Form::text('domain', (!empty($gitlabProject['domain']) ? $gitlabProject['domain'] : ''), ['class' => 'form-control', 'required'=>true]) !!}
            <div class="invalid-feedback">
                Please enter a Domain.
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::submit('Save Project',['class' => 'btn btn-primary ajaxForm']) !!}
</div>
{!! Form::close() !!}