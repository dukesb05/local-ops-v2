@extends('layouts.app')

@section('content')
<!-- Page Content -->
<div class="container">

	<!-- Page Heading -->
	<h1 class="my-4">Settings</h1>
	@if (count($settings) > 0)
	<div class="col-lg-12">
		<ul class="nav nav-tabs" role="tablist">
			@foreach ($settings as $section=>$fields)
			<li class="nav-item">
				<a class="nav-link{{$tab==$section ? ' active' : ''}}" href="#{{$section}}" id='{{$section}}-tab' data-toggle="tab" role="tab" aria-controls="{{$section}}" aria-selected="{{$tab==$section ? 'true' : 'false'}}">{{ucwords(str_replace('_', ' ', $section))}}</a>
			</li>
			@endforeach
		</ul>
		<div class="tab-content card card-body">
			@foreach ($settings as $section=>$fields)
			<div class="tab-pane fade show{{$tab==$section ? ' active' : ''}}" id="{{$section}}" role="tabpanel" aria-labelledby="{{$section}}-tab">
				{!! Form::open(['route' => ['settings.update', $section], 'class'=>'needs-validation']) !!}
				{!! Form::hidden('section', $section) !!}
				@foreach ($fields as $field=>$details)
				@php ($options = array_merge($details, ['id'=>$section.'_'.$field, 'class' => 'form-control folderSelect', 'webkitdirectory'=>true, 'directory'=>true, 'required'=>$details['required'], 'value'=>\App\Settings::$section($field)]))
				<div class="form-group">
					{!! Form::label($field, $details['name'], ['class' => 'col-form-label', 'required'=>$details['required']]) !!}
					{!! Form::field($field, $options) !!}
					<div class="invalid-feedback">
						Please enter a $details['name'].
					</div>
				</div>
				@endforeach
				<div class="form-group">
					{!! Form::submit('Save '.ucwords(str_replace('_', ' ', $section)).' Settings',['class' => 'btn btn-primary ajaxForm']) !!}
				</div>
				{!! Form::close() !!}
			</div>
			@endforeach
		</div>
	</div>
	<!-- /.row -->
	@else
	<p class="alert alert-info">There are no Settings set up yet.</p>
	@endif
</div>
<!-- /.container -->
@endsection