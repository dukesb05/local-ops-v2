<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::get('', 'ProjectController@index')->name('home');
Route::resource('settings', 'SettingsController');


Route::get('environments/{environment}/setup', 'EnvironmentController@setup')->name('environments.setup');
Route::get('environments/{environment}/setupStatus', 'EnvironmentController@runSetupStatus')->name('environments.setupStatus');
Route::post('environments/{environment}/setup', 'EnvironmentController@runSetup')->name('environments.runSetup');
Route::post('environments/{environment}/control/{action}', 'EnvironmentController@controlEnvironment')->name('environments.control');
Route::get('environments/{environment}/control/{action}', 'EnvironmentController@controlEnvironmentModaL')->name('environments.controlModal');
Route::resource('environments', 'EnvironmentController');
Route::get('treeview', 'TreeviewController@dirTree')->name('treeview');


Route::get('projects/groups', 'ProjectController@groups')->name('projects.groups');
Route::get('projects/gitlabSearch', 'ProjectController@gitlabSearch')->name('projects.gitlabSearch');
Route::get('projects/types', 'ProjectController@types')->name('projects.types');
Route::post('projects/types', 'ProjectController@storeType')->name('projects.types.store');
Route::get('projects/types/create', 'ProjectController@createType')->name('projects.types.create');
Route::get('projects/types/{type}', 'ProjectController@editType')->name('projects.types.show');
Route::post('projects/types/{type}', 'ProjectController@updateType')->name('projects.types.update');
Route::get('projects/{project}/domain/{domain}/launch', 'ProjectController@domainLaunch')->name('projects.launch');
Route::get('projects/{project}/tabInfo/{tab}', 'ProjectController@tabInfo')->name('projects.tabInfo');
Route::get('projects/{project}/setup', 'ProjectController@setup')->name('projects.setup');
Route::get('projects/{project}/setupStatus', 'ProjectController@runSetupStatus')->name('projects.setupStatus');
Route::post('projects/{project}/setup', 'ProjectController@runSetup')->name('projects.runSetup');
Route::get('projects/{project}/configure', 'ProjectController@configure')->name('projects.configure');
Route::get('projects/config-gen', 'ProjectController@generateConfigFile')->name('projects.config_gen');
Route::post('projects/{project}/actions/{action}', 'ProjectController@actions')->name('projects.actions');
Route::get('projects/{project}/contents/{content}', 'ProjectController@contents')->name('projects.contents');
Route::get('projects/configfiles', 'ProjectController@configfiles')->name('projects.configfiles');
Route::get('projects/configfiles/create', 'ProjectController@createConfigFile')->name('projects.configfiles.create');
Route::get('projects/configfiles/{configfile}', 'ProjectController@editConfigFile')->name('projects.configfiles.edit');
Route::post('projects/configfiles/{configfile}', 'ProjectController@updateConfigFile')->name('projects.configfiles.update');
Route::resource('projects', 'ProjectController');
Route::get('seo', 'SeoController@index')->name('seo.index');
Route::get('seo/linkChecker', 'SeoController@linkChecker')->name('seo.linkChecker');
Route::get('seo/imgChecker', 'SeoController@imgChecker')->name('seo.imgChecker');
Route::prefix('tools')->group(function () {
	Route::get('', function () {
		return view('tools');
	})->name('tools');
});
