<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Settings;
use Illuminate\Support\Facades\Input;
use Validator;
class SettingsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view('settings.index')
						->with('settings', Settings::fields())
						->with('tab', Input::get('tab', 'environment'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		return $this->save($request);
	}
	
	public function save(Request $request) {
		$return = ['status'=>'error', 'errFlds'=>[]];
		$tab = $request->input('section');
		$validator = Validator::make(Input::all(), Settings::validationRules($tab));
		if(!in_array($tab, array_keys(Settings::$fields))){
			$return['msg'] = 'Invalid Section';
		}elseif ($validator->fails()) {
			$return['errFlds'] = $validator->errors();
		} else {
			// store
			$data = [];
			foreach(Settings::$fields[$tab] as $field=>$options){
				$data[$field] = Input::get($field);
			}
			if($tab == 'gitlab'){
				foreach($data['connections'] as $conn_name => $connection){
					$data['connections'][$conn_name]['cache'] = !empty($connection['cache']) ? true : false;
				}
			}
			Settings::save($tab, $data);
			$return['status'] = 'success';
			$return['msg'] = ucwords($tab).' Settings Saved Successfully';
		}
		return response()->json($return);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

}
