<?php

namespace App\Http\Controllers;

use App\ResourceHacker;
use Illuminate\Http\Request;
use App\Environment;
use Validator;
use Illuminate\Support\Facades\Input;
use App\Settings;

class EnvironmentController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Settings::isSectionConfigured('environment')) {
            return redirect()->route('settings.index', ['tab' => 'environment']);
        }

        $environments = Environment::orderBy('name', 'asc')->get();

        return view('environments.index')
            ->with('environments', $environments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->ae(new Environment());
    }

    public function ae(Environment $environment)
    {
        $aeActions = $this->handleAeActions();
        if ($aeActions !== false) {
            return response()->json($aeActions);
        }

        return view('environments.ae')->with('environment', $environment);
    }

    public function handleAeActions()
    {
        if (!empty($_GET['projectAeAction'])) {
            switch ($_GET['projectAeAction']) {
                case 'php_extensions':
                    return false;
                    break;
            }
        }
        return false;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->save($request, new Environment());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Environment $environment)
    {
        if (!$environment->isSetup()) {
            return redirect()->route('environments.setup', ['environment' => $environment]);
        }
        return view('environments.view')->with('environment', $environment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Environment $environment)
    {
        return $this->ae($environment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $environment = Environment::find($id);
        return $this->save($request, $environment);
    }

    public function save(Request $request, Environment $environment)
    {
        $return = ['status' => 'error', 'errFlds' => []];
        $validator = Validator::make(Input::all(), $environment->validationRules);
        if ($validator->fails()) {
            $return['errFlds'] = $validator->errors();
        } else {
            // store
            $environment->name = Input::get('name');
            $environment->abbrev = Input::get('abbrev');
            $environment->apache_mods = implode(',', Input::get('apache_mods', []));
            $environment->php_extensions = implode(',', Input::get('php_extensions', []));
            $environment->php_version = Input::get('php_version');
            $environment->mysql_version = Input::get('mysql_version');
            $environment->save();
            if (!$environment->ip_address) {
                $environment->ip_address = '172.16.0.' . $environment->id;
                $environment->save();
            }
            $return['status'] = 'success';
            $return['msg'] = 'Environment Saved Successfully';
        }
        return response()->json($return);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function setup(Environment $environment)
    {
        return view('environments.setup')->with('environment', $environment);
    }

    public function runSetup(Environment $environment)
    {
        $environment->setupClass()->run();
    }

    public function runSetupStatus(Environment $environment)
    {
        return $environment->setupClass()->statusResponse();
    }

    public function controlEnvironment(Environment $environment, $action)
    {
        $response = ['status' => 'error'];
        switch ($action) {
            case 'start':
                $environment->start();
                $response['status'] = 'success';
                break;
            case 'stop':
                $environment->stop();
                $response['status'] = 'success';
                break;
            case 'restart':
                $environment->stop();
                $environment->start();
                $response['status'] = 'success';
                break;
            case 'remove':
                $environment->delete();
                $response['status'] = 'success';
                break;
            case 'uninstall':
                $environment->uninstall();
                $response['status'] = 'success';
                break;
            case 'terminal':
                $environment->runCommand(false, false);
                $response['status'] = 'success';
                break;
            case 'updateIcon':
                $environment->abbrev = Input::get('abbrev');
                $environment->save();
                $environment->generateIcon();
                ResourceHacker::addIcon($environment, $environment->folder().'/icon.ico');
                $response['status'] = 'success';
                $response['msg'] = 'Icon has been updated successfully';
                break;
        }
        return response()->json($response);
    }

    public function controlEnvironmentModal(Environment $environment, $action)
    {
        $response = 'Error';
        switch ($action) {
            case 'updateIcon':
                $response =  view('environments.modals.updateIcon')->with('environment', $environment);
                break;

        }
        return $response;
    }

}
