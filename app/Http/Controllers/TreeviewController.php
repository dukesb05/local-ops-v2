<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\DirTree;
class TreeviewController extends Controller {

	public function dirTree() {
		$startDir = Input::get('startDir', '/mnt');
		$htmlId = Input::get('htmlId');
		$directories = DirTree::directories($startDir, $startDir);
		$html = '<div class="list-group collapse show" id="' . $htmlId . '">';
		$id = str_replace(DirTree::folderHtmlId($startDir), '', $htmlId);
		if ($directories) {
			foreach ($directories as $directory) {
				$html .= view('form.dirTree.tree')->with('directory', $directory)->with('htmlId', $id.DirTree::folderHtmlId($directory['ubuntu']))->with('value', '');
			}
		} else {
			$html .= '<p class="alert alert-info">No Subdirectories</p>';
		}
		$html .= '</div>';
		return $html;
	}

}
