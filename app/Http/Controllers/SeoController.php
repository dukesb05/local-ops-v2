<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class SeoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		
	}

	public function linkChecker() {
		$url = Input::get('url');
		$links_missing_title = [];
		if ($url) {
			$arrContextOptions = [
				"ssl" => [
					"verify_peer" => false,
					"verify_peer_name" => false,
				],
			];
			$output = file_get_contents($url, false, stream_context_create($arrContextOptions));
			$DOM = new \DOMDocument;
			@$DOM->loadHTML($output);
			
			foreach ($DOM->getElementsByTagName('a') as $link) {
				if (!$link->getAttribute('title')) {
					$links_missing_title[] = [
						'href' => $link->getAttribute('href'),
						'nodeValue' => $link->nodeValue,
						'textContent' => $link->textContent,
						'line_num' => $link->getLineNo()
					];
				}
			}
		}
		return view('seo.linkChecker')->with('url', $url)->with('links_missing_title', $links_missing_title);
	}
	
	public function imgChecker() {
		$url = Input::get('url');
		$imgs_missing_title = [];
		if ($url) {
			$arrContextOptions = [
				"ssl" => [
					"verify_peer" => false,
					"verify_peer_name" => false,
				],
			];
			$output = file_get_contents($url, false, stream_context_create($arrContextOptions));
			$DOM = new \DOMDocument;
			@$DOM->loadHTML($output);
			
			foreach ($DOM->getElementsByTagName('img') as $link) {
				if (!$link->getAttribute('title')) {
					$imgs_missing_title[] = [
						'src' => $link->getAttribute('src'),
						'nodeValue' => $link->nodeValue,
						'textContent' => $link->textContent,
						'line_num' => $link->getLineNo()
					];
				}
			}
		}
		return view('seo.imgChecker')->with('url', $url)->with('imgs_missing_title', $imgs_missing_title);
	}

}
