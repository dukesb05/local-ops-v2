<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Settings;
use App\Project;
use App\Project\Type;
use App\Project\Group;
use App\Project\Domain;
use Validator;
use Illuminate\Support\Facades\Input;
use GrahamCampbell\GitLab\Facades\GitLab;
use Gitlab\ResultPager;
use App\DeployConfig;

class ProjectController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		if (!Settings::isSectionConfigured('project')) {
			return redirect()->route('settings.index', ['tab' => 'project']);
		}
		$projects = Project::orderBy('name', 'asc')->get();
		return view('projects.index')
						->with('projects', $projects);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(Project $project) {
		$aeActions = $this->handleAeActions();
		if ($aeActions !== false) {
			return response()->json($aeActions);
		}
		$repo_id		 = Input::get('repo_id');
		$gitlabProject	 = [];
		$reposDefault	 = [];
		$deployConfig	 = [];
		if ($repo_id) {
			GitLab::setUrl('https://git.d3corp.com');
			$gitlabProject					 = GitLab::projects()->show($repo_id);
			$gitlabProject['text']			 = $gitlabProject['path_with_namespace'];
			$gitlabProject['p_name']		 = ucwords(str_replace(['-', 'Site', 'site'], [' ', '', ''], $gitlabProject['path']));
			$gitlabProject['domain']		 = str_replace(['-', 'Site', 'site'], '', $gitlabProject['path']) . '.test';
			$gitlabProject['local_db_name']	 = str_replace(['-', 'Site', 'site'], '', $gitlabProject['path']);
			$gitlabProject['folder']		 = $gitlabProject['path'];
			$reposDefault[$repo_id]			 = $gitlabProject['text'];
			$deployConfig					 = DeployConfig::get($gitlabProject['path_with_namespace']);
		}
		$types			 = [];
		$project_types	 = Type::all();
		$selectedType	 = null;
		foreach ($project_types as $type) {
			$types[$type->id] = $type->name;
			if (!empty($deployConfig['project_type']) && in_array($deployConfig['project_type'], $type->getDeployConfigTypes())) {
				$selectedType = $type->id;
			}
		}
		return view('projects.create')->with('project', $project)->with('types', $types)->with('gitlabProject', $gitlabProject)->with('reposDefault', $reposDefault)->with('deployConfig', $deployConfig)->with('selectedType', $selectedType);
	}

	public function handleAeActions() {
		if (!empty($_GET['projectAeAction'])) {
			switch ($_GET['projectAeAction']) {
				case 'dbsourcesoptions':
					$optionsHtml = '';
					if (!empty($_GET['repo_path'])) {
						$deployConfig = DeployConfig::get($_GET['repo_path']);
						if (!empty($deployConfig['environments'])) {
							foreach ($deployConfig['environments'] as $env => $envInfo) {
								if (!empty($envInfo['db'])) {
									$optionsHtml .= '<option value="' . $env . '">' . $envInfo['label'] . '</option>';
								}
							}
						}
					}
					return $optionsHtml;
					break;
			}
		}
		return false;
	}

	public function createType() {
		return $this->aeType(new Type());
	}

	public function editType(Type $type) {
		return $this->aeType($type);
	}

	public function aeType(Type $type) {
		$envs			 = [];
		$environments	 = \App\Environment::all();
		foreach ($environments as $env) {
			$envs[$env->id] = $env->name;
		}
		$deployConfigTypes			 = DeployConfig::getProjectTypes();
		$selectedDeployConfigTypes	 = [];
		if ($type->id) {
			$selectedDeployConfigTypes = $type->getDeployConfigTypes();
		}
		return view('projects.types.ae')->with('project_type', $type)->with('envs', $envs)->with('deployConfigTypes', $deployConfigTypes)->with('selectedDeployConfigTypes', $selectedDeployConfigTypes);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		return $this->save($request, new Project());
	}

	public function storeType(Request $request) {
		return $this->saveType($request, new Type());
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show(Project $project) {
		if (!$project->isSetup()) {
			return redirect()->route('projects.setup', ['project' => $project]);
		}
		return view('projects.view')->with('project', $project);
	}

	public function showType(Type $type) {
		return view('projects.types.view')->with('type', $type);
	}

	public function setup(Project $project) {
		if ($project->isSetup()) {
			return redirect()->route('projects.show', ['project' => $project]);
		}
		return view('projects.setup')->with('project', $project);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Project $project) {
		$types			 = [];
		$project_types	 = Type::all();
		foreach ($project_types as $type) {
			$types[$type->id] = $type->name;
		}
		return view('projects.edit')->with('project', $project)->with('types', $types);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$project = Project::find($id);
		return $this->save($request, $project);
	}

	public function save(Request $request, Project $project) {
		$return		 = ['status' => 'error', 'errFlds' => []];
		$validator	 = Validator::make(Input::all(), $project->validationRules);
		if ($validator->fails()) {
			$return['errFlds'] = $validator->errors();
		} else {
			// store
			$repo_id = Input::get('repo_id');
			if ($repo_id) {
				GitLab::setUrl('https://git.d3corp.com');
				$gitlab_project		 = GitLab::projects()->show($repo_id);
				$group				 = Group::firstOrNew(['repo_id' => $gitlab_project['namespace']['id']]);
				$group->repo_id		 = $gitlab_project['namespace']['id'];
				$group->name		 = $gitlab_project['namespace']['name'];
				$group->folder		 = $gitlab_project['namespace']['path'];
				$group->save();
				$project->group_id	 = $group->id;
			}
			$project->type_id				 = Input::get('type_id');
			$project->repo_id				 = $repo_id;
			$project->name					 = Input::get('name');
			$project->folder				 = Input::get('folder');
			$project->repo_url				 = Input::get('repo_url');
			$project->save();
			$domain							 = Input::get('domain');
			$project_domain					 = Domain::firstOrNew(['project_id' => $project->id, 'domain' => $domain]);
			$project_domain->project_id		 = $project->id;
			$project_domain->environment_id	 = 1;
			$project_domain->domain			 = $domain;
			$project_domain->main			 = 1;
			$project_domain->ssl			 = 1;
			$project_domain->save();
			$return['status']				 = 'success';
			$return['msg']					 = 'Project Saved Successfully';
		}
		return response()->json($return);
	}

	public function updateConfigFile(Request $request, $id = null) {
		$configFile	 = Project\ConfigFile::find($id);
		$return		 = ['status' => 'error', 'errFlds' => []];
		$validator	 = Validator::make(Input::all(), $configFile->validationRules);
		if ($validator->fails()) {
			$return['errFlds'] = $validator->errors();
		} else {
			// store
			$configFile->name			 = Input::get('name');
			$configFile->save();
			$deploy_config_build_tasks	 = Input::get('deploy_config_build_tasks', []);
			\DB::table('projects_config_files_build_tasks')->where('config_file_id', '=', $configFile->id)->delete();
			foreach ($deploy_config_build_tasks as $deployConfigBuildTask => $value) {
				\DB::table('projects_config_files_build_tasks')->insert(
						['config_file_id' => $configFile->id, 'build_task' => $deployConfigBuildTask]
				);
			}
			$return['status']	 = 'success';
			$return['msg']		 = 'Project Config File Saved Successfully';
		}
		return response()->json($return);
	}

	public function updateType(Request $request, $id = null) {
		$type = Type::find($id);
		return $this->saveType($request, $type);
	}

	public function saveType(Request $request, Type $type) {
		$return		 = ['status' => 'error', 'errFlds' => []];
		$validator	 = Validator::make(Input::all(), $type->validationRules);
		if ($validator->fails()) {
			$return['errFlds'] = $validator->errors();
		} else {
			// store
			$type->name				 = Input::get('name');
			$type->default_env		 = Input::get('default_env');
			$type->composer_cmd		 = Input::get('composer_cmd');
			$type->requires_local_db = Input::get('requires_local_db', 0) ? 1 : 0;
			$type->save();
			$deploy_config_types	 = Input::get('deploy_config_types', []);
			\DB::table('projects_types_deploy_config_types')->where('project_type_id', '=', $type->id)->delete();
			foreach ($deploy_config_types as $deployConfigType => $value) {
				\DB::table('projects_types_deploy_config_types')->insert(
						['project_type_id' => $type->id, 'deploy_config_key' => $deployConfigType]
				);
			}
			$return['status']	 = 'success';
			$return['msg']		 = 'Project Saved Successfully';
		}
		return response()->json($return);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Project $project) {
		//remove directory
		exec("rm -rf {$project->directory()}", $output);

		//remove hosts entries and apache files
		foreach ($project->domains as $domain) {
			$env = $domain->environment_id > 0 ? \App\Environment::find($domain->environment_id) : $project->environment();
			if (!$env->isRunning()) {
				$env->start();
			}
			if ($env->isRunning()) {
				$host_entry_sed = '/' . $env->ip_address . ' ' . $domain->domain . '/d';
				exec("sudo sed -i {$host_entry_sed} /mnt/c/Windows/System32/drivers/etc/hosts");
				$env->runCommand("sudo sed -i {$host_entry_sed} /etc/hosts");
				$env->runCommand("sudo a2dissite {$project->apacheFileName()}");
				$env->runCommand("sudo service apache2 reload");
				$env->runCommand("sudo rm /etc/apache2/sites-available/{$project->apacheFileName()}.conf");
			}
		}

		$project->delete();
		return redirect()->route('projects.index');
	}

	public function types() {
		if (!Settings::isSectionConfigured('project')) {
			return redirect()->route('settings.index', ['tab' => 'project']);
		}
		$project_types = Type::orderBy('name', 'asc')->get();
		return view('projects.types.index')
						->with('project_types', $project_types);
	}

	public function configfiles() {
		if (!Settings::isSectionConfigured('project')) {
			return redirect()->route('settings.index', ['tab' => 'project']);
		}
		$configFiles = Project\ConfigFile::orderBy('name', 'asc')->get();
		return view('projects.configfiles.index')
						->with('configFiles', $configFiles);
	}

	public function editConfigFile(Project\ConfigFile $configfile) {
		$deployConfigBuildTasks			 = DeployConfig::getBuildTasks();
		$selectedDeployConfigBuildTasks	 = [];
		if ($configfile->id) {
			$selectedDeployConfigBuildTasks = $configfile->getDeployConfigBuildTasks();
		}
		return view('projects.configfiles.ae')->with('configfile', $configfile)->with('deployConfigBuildTasks', $deployConfigBuildTasks)->with('selectedDeployConfigBuildTasks', $selectedDeployConfigBuildTasks);
	}

	public function gitlabSearch() {
		$q					 = request()->get('q');
		GitLab::setUrl('https://git.d3corp.com');
		$results			 = GitLab::projects()->all(['search' => $q]);
		$installedProjects	 = Project::all()->pluck('id', 'repo_id')->toArray();
		$return				 = ['results' => []];
		foreach ($results as $result) {
			$result['text']			 = $result['path_with_namespace'];
			$result['p_name']		 = ucwords(str_replace(['-', 'Site', 'site'], [' ', '', ''], $result['path']));
			$result['domain']		 = str_replace(['-', 'Site', 'site'], '', $result['path']) . '.test';
			$result['local_db_name'] = str_replace(['-', 'Site', 'site'], '', $result['path']);
			$result['folder']		 = $result['path'];
			$result['local_id']		 = in_array($result['id'], array_keys($installedProjects)) ? $installedProjects[$result['id']] : false;
			$return['results'][]	 = $result;
		}
		return response()->json($return);
	}

	public function runSetup(Project $project) {
		$project->setupClass()->run();
	}

	public function runSetupStatus(Project $project) {
		return $project->setupClass()->statusResponse();
	}

	public function domainLaunch(Project $project, Domain $domain) {
		if (!$domain->environment()->isRunning()) {
			$domain->environment()->start();
		}
		$protocol = $domain->ssl ? 'https://' : 'http://';
		return redirect($protocol . $domain->domain);
	}

	public function generateConfigFile($project_id = false, $file_type = false, $returnHtml = false) {
		$projects	 = Project::orderBy('name', 'asc')->get();
		$projs		 = [];
		foreach ($projects as $project) {
			$projs[$project->id] = $project->name;
		}
		$file_types	 = ['wp_env' => 'Wordpress (.env)', 'wp_php' => 'Wordpress (.php)', 'bs' => 'Basesite', 'mag' => 'Magento'];
		$project_id	 = Input::get('project_id') ? Input::get('project_id') : $project_id;
		$file_type	 = Input::get('file_type') ? Input::get('file_type') : $file_type;
		$config_file = '';
		if ($project_id && $file_type) {
			$project		 = Project::find($project_id);
			$deployConfig	 = DeployConfig::get($project->repoPath());
			if (!isset($deployConfig['environments'])) {
				$deployConfig['environments'] = [];
			}
			$config_file = view('projects.config_' . $file_type)->with('project', $project)->with('deployConfig', $deployConfig);
		}
		$view = view('projects.config_gen')
				->with('projects', $projs)
				->with('file_types', $file_types)
				->with('config_file', $config_file)
				->with('project_id', $project_id)
				->with('file_type', $file_type);
		if ($returnHtml) {
			$sections = $view->renderSections(); // returns an associative array of 'content', 'head' and 'footer'
			return $sections['content']; // this will only return whats in the content section
		}
		return $view;
	}

	public function actions(Project $project, $action) {
		$response = ['status' => 'error', 'msg' => 'Invalid Action'];
		switch ($action) {
			case 'openFolder':
				$project->openFolder();
				$response['status']	 = 'success';
				$response['msg']	 = 'Folder opened successfully.';
				break;
			case 'openNetbeans':
				$project->openNetbeansProject();
				$response['status']	 = 'success';
				$response['msg']	 = 'Netbeans project opened successfully.';
				break;
			case 'openTerminal':
				$project->openTerminal();
				$response['status']	 = 'success';
				$response['msg']	 = 'Terminal opened successfully.';
				break;
			case 'updateComposer':
				$project->updateComposer();
				$response['status']	 = 'success';
				$response['msg']	 = 'Composer updated successfully.';
				break;
			case 'apacheLogs':
				$project->apacheLogs();
				$response['status']	 = 'success';
				$response['msg']	 = 'Apache logs opened successfully.';
				break;
			case 'clearApacheLogs':
				$project->clearApacheLogs();
				$response['status']	 = 'success';
				$response['msg']	 = 'Apache logs cleared successfully.';
				break;
		}
		return response()->json($response);
	}

	public function contents(Project $project, $content) {
		$response = ['status' => 'error', 'html' => 'Invalid Content'];
		switch ($content) {
			case 'configFile':
				$response['status']	 = 'success';
				$response['html']	 = $this->displayConfigFile($project);
				break;
			case 'domains':
				$response['status']	 = 'success';
				$view				 = view('projects.contents.domains')
						->with('project', $project);
				$response['html']	 = $view->render();
				break;
			case 'htaccessMedia':
				$response['status']	 = 'success';
				$deployConfig		 = DeployConfig::get($project->repoPath());
				if (!isset($deployConfig['environments'])) {
					$deployConfig['environments'] = [];
				}
				$view				 = view('projects.contents.htaccess_media')
						->with('project', $project)
						->with('deployConfig', $deployConfig);
				$response['html']	 = $view->render();
				break;
		}
		return response()->json($response);
	}

	public function displayConfigFile(Project $project) {
		$deployConfig = DeployConfig::get($project->repoPath());
		DeployConfig::addLocalEnv($deployConfig, $project->mainDomain()->domain);
		$config_file = false;
		if (!empty($deployConfig['build_tasks'])) {
			$setBuildTasks = Project\ConfigFile::allBuildTasks();
			foreach ($deployConfig['build_tasks'] as $task) {
				if (isset($setBuildTasks[$task])) {
					$config_file = Project\ConfigFile::find($setBuildTasks[$task]);
					break;
				}
			}
		}

		if ($config_file) {
			$file_type = $config_file->key;
			return view('projects.config_' . $file_type)->with('project', $project)->with('deployConfig', $deployConfig)->render();
		}

		return $this->generateConfigFile();
	}

}
