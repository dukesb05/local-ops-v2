<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Environment extends Model
{

    public $timestamps = false;
    public $validationRules = [
        'name' => 'required',
        'php_version' => 'required',
        'mysql_version' => 'required'
    ];

    /** @var App\Environments\Setup $setupClass * */
    private $setupClass = null;
    private $isRunning = null;
    private $apacheIsRunning = null;
    private $mysqlIsRunning = null;

    public function link($method = 'show', $params = [])
    {
        $params = array_merge(['id' => $this->id], $params);
        return route('environments.' . $method, $params);
    }

    public function isInstalled()
    {
        return file_exists($this->instanceFile());
    }

    public function isRunning()
    {
        if (!$this->isInstalled()) {
            return false;
        }
        if (is_null($this->isRunning)) {
            $this->isRunning = $this->apacheIsRunning();
        }
        return $this->isRunning;
    }

    public function apacheIsRunning()
    {
        if (!$this->isInstalled()) {
            return false;
        }
        if (is_null($this->apacheIsRunning)) {
            $this->apacheIsRunning = false;
            exec("sudo ssh root@{$this->ip_address} 'service apache2 status' 2>&1", $output);
            if (!empty($output[0])) {
                if(strpos($output[0], 'Connection refused') !== false){
                    $this->runCommand('service ssh start');
                }
                $this->apacheIsRunning = strpos($output[0], 'apache2 is running') !== false;
            }
        }
        return $this->apacheIsRunning;
    }

    public function mysqlIsRunning()
    {
        if (!$this->isInstalled()) {
            return false;
        }
        if (is_null($this->mysqlIsRunning)) {
            $this->mysqlIsRunning = false;
            exec("sudo ssh root@{$this->ip_address} 'service mysql status' 2>&1", $output);
            if(strpos($output[0], 'Connection refused') !== false){
                $this->runCommand('service ssh start');
            }
            if (!empty($output[0]) && strpos($output[0], 'Permission denied') === false) {
                $this->mysqlIsRunning = !(strpos($output[0], 'MySQL is stopped') !== false);
            }
        }
        return $this->mysqlIsRunning;
    }

    public function start()
    {
        $this->runCommand('service apache2 start');
        //$this->runCommand('service mysql start');
        $this->runCommand('service php5.6-fpm start');
		$this->runCommand('service mailhog start');
    }

    public function stop()
    {
        $this->runCommand('service apache2 stop');
        //$this->runCommand('service mysql stop');
        $this->runCommand('service php5.6-fpm stop');
		$this->runCommand('service mailhog stop');
    }

    public function runCommand($cmd = false, $wait = true)
    {
        $ending = $wait ? '2>&1' : '> /dev/null 2>/dev/null &';
        $windowsInstance = str_replace('\\', '\\\\', \App\DirTree::directoryToWindows($this->instanceFile()));
        if ($cmd) {
            //echo "/mnt/c/Windows/System32/cmd.exe /c start {$windowsInstance} run \"{$cmd}\" 2>&1";
            //die();
            $output = shell_exec("/mnt/c/Windows/System32/cmd.exe /c start {$windowsInstance} run \"{$cmd}\" $ending");
        } else {
            $output = shell_exec("/mnt/c/Windows/System32/cmd.exe /c start {$windowsInstance} $ending");
            //$output = shell_exec("/mnt/c/Windows/System32/cmd.exe /c start {$windowsInstance} > /dev/null 2>/dev/null &");
        }
        return $output;
    }

    public function runCommandBackground($cmd = false)
    {
        $windowsInstance = str_replace('\\', '\\\\', \App\DirTree::directoryToWindows($this->instanceFile()));
        if ($cmd) {
            echo "/mnt/c/Windows/System32/cmd.exe {$windowsInstance} run \"{$cmd}\" 2>&1";
            $output = shell_exec("/mnt/c/Windows/System32/cmd.exe {$windowsInstance} run \"{$cmd}\" 2>&1");
        } else {
            $output = shell_exec("/mnt/c/Windows/System32/cmd.exe {$windowsInstance} > /dev/null 2>/dev/null &");
        }
        return $output;
    }

    public function isSetup()
    {
        if (is_dir($this->folder())) {
            return true;
        }
        return false;
    }

    public function cleanName()
    {
        return str_replace(' ', '_', $this->name);
    }

    public function folder()
    {
        return Settings::environment('root_dir') . '/' . $this->cleanName();
    }

    public function instanceFile()
    {
        return $this->folder() . '/' . $this->cleanName() . '.exe';
    }

    /**
     *
     * @return App\Environment\Setup
     */
    public function setupClass()
    {
        if (is_null($this->setupClass)) {
            $this->setupClass = new \App\Environment\Setup($this);
        }
        return $this->setupClass;
    }

    public function delete()
    {
        $this->uninstall();
        return parent::delete();
    }

    public function uninstall(){
        $windowsInstance = str_replace('\\', '\\\\', \App\DirTree::directoryToWindows($this->instanceFile()));
        shell_exec("/mnt/c/Windows/System32/cmd.exe /c start {$windowsInstance} clean 2>&1");
        shell_exec("sudo rm -rf {$this->folder()}");
    }

    public function generateIcon(){
        $im = imagecreatetruecolor(256, 256);
        $bg = imagecolorallocate($im, 43,158,235);
        $textshadow = imagecolorallocate($im, 128, 128, 128);
        $textcolor = imagecolorallocate($im, 240,248,253);
        imagefilledrectangle($im, 0, 0, 256, 256, $bg);
        $text = $this->abbrev;
        $font = '/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf';
        switch(strlen($text)){
            case 3:
                $shadow_x = -10;
                $text_x = -8;
                break;
            case 2:
                $shadow_x = 39;
                $text_x = 41;
                break;
            case 1:
                $shadow_x = 84;
                $text_x = 86;
                break;
        }
        imagettftext($im, 100, 0, $shadow_x, 178, $textshadow, $font, $text);
        imagettftext($im, 100, 0, $text_x, 176, $textcolor, $font, $text);
        imagepng($im, $this->folder().'/icon_256.png');

        //resize images using imagemagick
        shell_exec("convert {$this->folder()}/icon_256.png -resize 16x16 {$this->folder()}/icon_16.png 2>&1");
        shell_exec("convert {$this->folder()}/icon_256.png -resize 32x32 {$this->folder()}/icon_32.png 2>&1");
        shell_exec("convert {$this->folder()}/icon_256.png -resize 64x64 {$this->folder()}/icon_64.png 2>&1");
        shell_exec("convert {$this->folder()}/icon_256.png -resize 128x128 {$this->folder()}/icon_128.png 2>&1");
        shell_exec("convert {$this->folder()}/icon_16.png {$this->folder()}/icon_32.png {$this->folder()}/icon_64.png {$this->folder()}/icon_128.png {$this->folder()}/icon_256.png -colors 256 {$this->folder()}/icon.ico 2>&1");
    }

}
