<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
	public $bindings = [
        'form' => \App\FormBuilder::class,
    ];
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
       $this->app->extend('form', function ($command, $app) {
            return new \App\Form($app['html'], $app['url'], $app['view'], csrf_token(), $app['request']);
        });

        $this->app->alias('form', \App\Form::class);
		$this->app->bind('path.public', function() {
			return base_path().'/html';
		});
    }
}
