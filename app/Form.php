<?php

namespace App;

use Collective\Html\FormBuilder;
use BadMethodCallException;

class Form extends FormBuilder {

	public function dirTree($name, $options = []) {
		$startDir	 = !empty($options['value']) ? $options['value'] : '/mnt';
		$directories = DirTree::directories($startDir);
		return view('form.dirTree')->with('directories', $directories)->with('name', $name)->with('value', DirTree::directoryToWindows($startDir))->with('id', $options['id']);
	}

	public function gitlabConnections($name, $options = []) {
		$connections = !empty($options['connections']) ? $options['connections'] : [];
		return view('form.gitlab.connections')->with('connections', $connections)->with('name', $name);
	}

	public function gitlabSshKey($name, $options = []) {
		$sshKey = SshKey::get();
		return view('form.gitlab.sshkey')->with('name', $name)->with('sshKey', $sshKey);
	}

	public function field($name, $options = []) {
		if (!empty($options['type'])) {
			$type = $options['type'];
			unset($options['type']);
			if (method_exists($this, $type)) {
				switch ($type) {
					case 'select':
						$list				 = $selectAttributes	 = $optionsAttributes	 = $optgroupsAttributes = [];
						$selected			 = null;
						foreach (['list', 'selectAtts', 'optionsAtts', 'optgroupsAtts', 'selected'] as $field) {
							if (!empty($options[$field])) {
								${$field} = $options[$field];
								unset($options[$field]);
							}
						}
						unset($options['name']);
						if ($options) {
							$selectAttributes = array_merge($selectAttributes, $options);
						}
						return $this->select($name, $list, $selected, $selectAttributes, $optionsAttributes, $optgroupsAttributes);
					default:
						return $this->$type($name, $options);
				}
			}
		}
		throw new BadMethodCallException("Method {$type} does not exist.");
	}

	public function checkboxes($name, $list, $checkedValues, $checkboxAttributes = []) {
		$defaultAtts = ['class'=>'checkbox'];
		$checkboxAttributes += $defaultAtts;
		$html = '';
		$name = str_replace('[]', '', $name);
		foreach ($list as $value => $label) {		
			$fieldName = $name.'['.$value.']';
			$html .= '<div class="form-check-inline">' . '<label class="form-check-label">' . $this->checkbox($fieldName, $value, in_array($value, $checkedValues), $checkboxAttributes) . $label . '</label></div>';
		}
		return $html;
	}

}
