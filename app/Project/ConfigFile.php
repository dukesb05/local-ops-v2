<?php

namespace App\Project;

use Illuminate\Database\Eloquent\Model;

class ConfigFile extends Model {

	protected $table = 'projects_config_files';
	protected $guarded = [];
	public $validationRules = [
		'name' => 'required'
	];

	public function link($method = 'edit', $params = []) {
		$params = array_merge(['id' => $this->id], $params);
		return route('projects.configfiles.' . $method, $params);
	}
	
	public function getDeployConfigBuildTasks() {
		if ($this->id) {
			return \DB::table('projects_config_files_build_tasks')->where('config_file_id', $this->id)->pluck('build_task')->toArray();
		}
		return [];
	}
	
	public static function allBuildTasks(){
		return \DB::table('projects_config_files_build_tasks')->pluck('config_file_id', 'build_task')->toArray();
	}
}
