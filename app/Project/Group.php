<?php

namespace App\Project;

use Illuminate\Database\Eloquent\Model;

class Group extends Model {

	protected $table = 'projects_groups';
	protected $guarded = [];
	public $validationRules = [
		'repo_id' => 'required',
		'name' => 'required',
		'folder' => 'required'
	];
	
	public function link($method = 'show', $params = []) {
		$params = array_merge(['id' => $this->id], $params);
		return route('projects.groups.' . $method, $params);
	}
	
	public function directory(){
		return \App\Settings::project('root_dir').DIRECTORY_SEPARATOR.$this->folder;
	}
}
