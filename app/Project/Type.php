<?php

namespace App\Project;

use Illuminate\Database\Eloquent\Model;

class Type extends Model {

	protected $table		 = 'projects_types';
	protected $guarded		 = [];
	public $validationRules	 = [
		'name'			 => 'required',
		'default_env'	 => 'required',
		'composer_cmd'	 => 'required'
	];

	public function link($method = 'show', $params = []) {
		$params = array_merge(['id' => $this->id], $params);
		return route('projects.types.' . $method, $params);
	}

	public function getDeployConfigTypes() {
		if ($this->id) {
			return \DB::table('projects_types_deploy_config_types')->where('project_type_id', $this->id)->pluck('deploy_config_key')->toArray();
		}
		return [];
	}

}
