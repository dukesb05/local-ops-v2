<?php

namespace App\Project;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model {

	protected $table = 'projects_domains';
	protected $guarded = [];
	public $validationRules = [
		'project_id' => 'required',
		'domain' => 'required'
	];
	protected $environment;
	public function link($method = 'show', $params = []) {
		$params = array_merge(['id' => $this->id], $params);
		return route('projects.groups.' . $method, $params);
	}
	
	/**
	 * 
	 * @return \App\Environment
	 */
	public function environment() {
		if (is_null($this->environment)) {
			$this->environment = \App\Environment::find($this->environment_id);
		}
		return $this->environment;
	}
}
