<?php

namespace App\Project;

use App\Setup as BaseSetup;
use App\Project;
use Illuminate\Support\Facades\Storage;

class Setup extends BaseSetup {

    protected $steps = [
        'repo' => 'Install Site Files',
        'composer' => 'Run Composer Commands',
        'hosts' => 'Update Hosts File',
        'apache' => 'Update Apache Config',
        'database' => 'Sync Database',
		'netbeans' => 'Setup Netbeans Project'
    ];
    private $project;

    public function __construct(Project $project) {
        $this->project = $project;
    }

    public function getSteps() {
        return $this->steps;
    }

    public function run() {
        $this->setCurrentMsg('Starting Setup');
        foreach (array_keys($this->steps) as $step) {
            $this->$step();
        }

        $this->setStatus('completed');
    }

    public function repo() {
        $this->setCurrentMsg('Installing Site Files from Repository');
        if (!is_dir($this->project->directory())) {
			$this->needEnv($this->project->environment());
            if ($this->project->environment()->isRunning()) {
                $windowsInstance = str_replace('\\', '\\\\', \App\DirTree::directoryToWindows($this->project->environment()->instanceFile()));
                $output = shell_exec("/mnt/c/Windows/System32/cmd.exe /c start {$windowsInstance} run git clone -b stable {$this->project->repo_url} {$this->project->directory()} 2>&1");
            }
            //exec("sudo git clone {$this->project->repo_url} {$this->project->directory()}", $output);
            //switch to stable branch
            //exec("cd {$this->project->directory()} && git checkout -b stable 2>&1", $output);

        }
        $folder = \App\DirTree::directoryToWindows($this->project->directory());
        $this->addMsg("Successfully Installed Site Files to {$folder}");
    }

    public function composer() {
        $this->setCurrentMsg('Running Composer Commands if Necessary');
        if (file_exists($this->project->directory() . '/composer.json')) {
            $this->needEnv($this->project->environment());
            if ($this->project->environment()->isRunning()) {
                $windowsInstance = str_replace('\\', '\\\\', \App\DirTree::directoryToWindows($this->project->environment()->instanceFile()));
                $output = shell_exec("cd {$this->project->directory()} && /mnt/c/Windows/System32/cmd.exe /c start {$windowsInstance} run composer update 2>&1");
            }
        }
        $this->addMsg("Successfully Ran Composer Commands");
    }

    public function hosts() {
        $this->setCurrentMsg('Updating hosts file if necessary');
        foreach ($this->project->domains as $domain) {
            $env = $domain->environment_id > 0 ? \App\Environment::find($domain->environment_id) : $this->project->environment();
            $this->needEnv($env);
            if ($env->isRunning()) {
                $host_entry = $env->ip_address . ' ' . $domain->domain;
                $this->setCurrentMsg('Checking for hosts file');
                exec("sudo grep -R '{$host_entry}' /mnt/c/Windows/System32/drivers/etc/hosts 2>&1", $host);
                if (!empty($host[0]) && $host[0] == $host_entry) {
                    $this->setCurrentMsg('Hosts File Already Set, Moving on');
                    //no update needed
                } else {
                    $this->setCurrentMsg('Setting hosts file for windows and environment.');
                    exec("sudo printf '\n{$host_entry}' >> /mnt/c/Windows/System32/drivers/etc/hosts 2>&1", $win_host_output);
                    $env->runCommand("sudo printf '\\n{$host_entry}' >> /etc/hosts");
					shell_exec("sudo bash -c 'printf \"\n{$host_entry}\" >> /etc/hosts' 2>&1");
                }
            }
        }
        $this->addMsg("Successfully updated hosts files");
    }

    public function apache() {
        $this->setCurrentMsg('Generating Apache File');
        $this->needEnv($this->project->environment());
        if ($this->project->environment()->isRunning()) {
            //on setup only need to setup main domain, other domains will be set up as added
            $this->setCurrentMsg('Generating Virtual Host File');
            $virtualHost = $this->project->apacheFile();
            $this->setCurrentMsg('Successfully Generated Host File');

            $this->setCurrentMsg('Saving File on Server');
            Storage::disk('local')->put("apache/{$this->project->apacheFileName()}.conf", $virtualHost);
            $this->setCurrentMsg('Successfully Created File on Server');

            $this->setCurrentMsg('Copying file to Environment');
            $storagePath = rtrim(Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix(), '/');
            $storagePath .= "/apache/{$this->project->apacheFileName()}.conf";
            $this->project->environment()->runCommand("cp {$storagePath} /etc/apache2/sites-available/{$this->project->apacheFileName()}.conf");
            $this->setCurrentMsg('Successfully copied file to Environment');

            $this->setCurrentMsg('Adding site to apache');
            $this->project->environment()->runCommand("sudo a2ensite {$this->project->apacheFileName()}");
            $this->setCurrentMsg('Successfully added site to apache', 'success');

            $this->setCurrentMsg('Reloading Apache for new config changes');
            $this->project->environment()->runCommand("sudo service apache2 reload");
            $this->setCurrentMsg('Successfully Reloaded apache', 'success');
        }
        $this->addMsg("Successfully setup Apache");
    }

    public function database() {
        
    }
	
	public function netbeans(){
		if (!is_dir($this->project->directory() . '/nbproject')) {
			//create nbproject folder
			$folder =  \Storage::disk('local')->path('netbeans');
			shell_exec("cp -R {$folder}/* {$this->project->directory()}");
			shell_exec("sed -i 's/{{PROJECT_NAME}}/{$this->project->name}/g' {$this->project->directory()}/nbproject/project.xml");
			shell_exec("sed -i 's/{{DOMAIN}}/{$this->project->mainDomain()->domain}/g' {$this->project->directory()}/nbproject/private/private.properties");
		}
	}

    protected function needEnv($env) {
        if (!$env->isRunning()) {
            $this->setCurrentMsg('Environment is not running, starting environment');
            $env->start();
            $this->setCurrentMsg('Started Environment');
        }
    }

}
