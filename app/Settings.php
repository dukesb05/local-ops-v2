<?php

namespace App;

use Illuminate\Support\Facades\Config;
class Settings {

	public static $fields = [
		'environment' => [
			'root_dir' => ['required' => true, 'name' => 'Root Environments Dir', 'type'=>'dirTree']
		],
		'project' => [
			'root_dir' => ['required' => true, 'name' => 'Root Project Dir', 'type'=>'dirTree']
		],
		'gitlab' => [
			'default' => ['required' => true, 'name' => 'Default Connection', 'type'=>'select'],
			'connections' => ['required' => true, 'name'=> 'Connections', 'type'=>'gitlabConnections'],
			'ssh_key' => ['required' => true, 'name'=> 'SSH Key', 'type'=>'gitlabSshKey']
		]
	];
	
	public static function fields(){
		$connections = self::gitlab('connections');
		self::$fields['gitlab']['connections']['connections'] = $connections;
		$connections = array_combine(array_keys($connections), array_map('ucwords', array_keys($connections)));
		self::$fields['gitlab']['default']['list'] = $connections;
		return self::$fields;
	}
	
	public static function validationRules($section){
		$validationRules = [];
		if(isset(self::$fields[$section])){
			foreach(self::$fields[$section] as $field=>$options){
				if($options['required']){
					$validationRules[$field] = 'required';
				}
			}
		}
		return $validationRules;
	}
	
	public static function environment($key = false) {
		return static::get('environment', $key);
	}
	
	public static function project($key = false) {
		return static::get('project', $key);
	}
	
	public static function gitlab($key = false) {
		$config_str = 'gitlab'. ($key ? '.' . $key : '');
		return Config::get($config_str);
	}

	public static function get($section = false, $key = false) {
		$config_str = 'settings' . ($section ? '.' . $section : '') . ($key ? '.' . $key : '');
		return Config::get($config_str);
	}

	public static function isSectionConfigured($section) {
		if (!isset(static::$fields[$section])) {
			return false;
		}

		foreach (static::$fields[$section] as $field => $fld_details) {
			if ($fld_details['required'] && !static::$section($field)) {
				return false;
			}
		}

		return true;
	}
	
	public static function save($section, $data){
		$config = [];
		foreach(self::$fields[$section] as $field=>$options){
			$prefix = $section == 'gitlab' ? '' : 'settings.';
			if(is_array($data[$field])){
				$config_key = $prefix.$section.'.'.$field;
				foreach($data[$field] as $key => $value){
					if(is_array($value)){
						foreach($value as $key2=>$val){
							Config::write($config_key.'.'.$key.'.'.$key2, $val);
						}
					}else{
						Config::write($config_key.'.'.$key, $value);
					}
				}
			}else{
				Config::write($prefix.$section.'.'.$field, $data[$field]);
			}
		}
		return true;
	}

}
