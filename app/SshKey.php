<?php
namespace App;
class SshKey{
	public static function get(){
		exec('sudo cat /root/.ssh/id_rsa.pub 2>&1', $sshKey);
		if(isset($sshKey[0]) && strpos($sshKey[0], 'No such file or directory') === false){
			return $sshKey[0];
		}
		return '';
	}
}