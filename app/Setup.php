<?php
namespace App;

class Setup{
	protected $response = ['runSetup'=>['status'=>'running', 'output'=>[], 'currentMsg'=>'Running Setup...']];
	
	public function __construct() {
		$this->saveStatus();
	}
	
	public function saveStatus(){
		session($this->response);
		session()->save();
	}
	
	public function addMsg($msg, $status = 'success'){
		$this->response['runSetup']['output'][] = ['status'=>$status, 'msg'=> $msg];
		$this->saveStatus();
	}
	
	public function setCurrentMsg($msg){
		$this->response['runSetup']['currentMsg'] = $msg;
		$this->saveStatus();
	}
	
	public function setStatus($status){
		$this->response['runSetup']['status'] = $status;
		$this->saveStatus();
	}
	
	public function statusResponse(){
		$runSetup = session('runSetup');
		$status = $runSetup['status'];
		$currentMsg = $runSetup['currentMsg'];
		$html = '<ul class="list-group">';
		foreach($runSetup['output'] as $output){
			$html .= '<li class="list-group-item list-group-item-'.$output['status'].'">'.$output['msg'].'</li>';
		}
		$html .= '</ul>';
		return response()->json(['status'=>$status, 'html'=>$html, 'currentMsg'=>$currentMsg]);
	}
}