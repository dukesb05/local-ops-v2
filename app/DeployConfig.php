<?php

namespace App;

use Yaml;

class DeployConfig {

	protected static $repoUrl	 = 'git@git.d3corp.com:d3corp/deploy-config.git';
	protected static $folder	 = 'deploy-config';
	protected static $envOrder	 = [1 => 'production', 2 => 'staging'];

	public static function get($repoPath) {
		self::update();
		$projectConfig = [];
		if (is_dir(self::path($repoPath))) {
			$projectConfig = Yaml::parse(self::getConfig(self::$folder . '/' . $repoPath . '/_config.yml'));
			if ($projectConfig && is_dir(self::path($repoPath . '/environments'))) {
				$files = \File::allFiles(self::path($repoPath . '/environments'));
				foreach ($files as $file) {
					$file_split							 = explode('/', (string) $file);
					$file								 = end($file_split);
					$env								 = str_replace('.yml', '', $file);
					$projectConfig['environments'][$env] = Yaml::parse(self::getConfig(self::$folder . '/' . $repoPath . '/environments/' . $file));
				}
			}
		}
		if (isset($projectConfig['environments'])) {
			uksort($projectConfig['environments'], '\App\DeployConfig::sortEnvironments');
		} else {
			$projectConfig['environments'] = [];
		}
		return $projectConfig;
	}

	public static function addLocalEnv(&$projectConfig, $localUrl) {
		$envToCopyFrom = !empty($projectConfig['environments']['production']) ? $projectConfig['environments']['production'] : $projectConfig['environments']['staging'];
		$localEnv = [
			'local' => [
				'label' => 'Local',
				'db' => [
					'name' => str_replace('.test', '', $localUrl),
					'user' => 'root',
					'pass' => 'root',
					'host' => '127.0.0.1',
					'prefix' => !empty($envToCopyFrom['db']['prefix']) ? $envToCopyFrom['db']['prefix'] : ''
				],
				'wpconfig' => [
					'env' => 'development'
				]
			]
		];
		$localEnv['local'] = array_replace_recursive($envToCopyFrom, $localEnv['local']);
		$projectConfig['environments'] = $localEnv + $projectConfig['environments'];
	}

	public static function sortEnvironments($a, $b) {
		if (in_array($a, static::$envOrder) && in_array($b, static::$envOrder)) {
			return (array_search($a, static::$envOrder) > array_search($b, static::$envOrder));
		} elseif (in_array($a, static::$envOrder)) {
			return array_search($a, static::$envOrder);
		} elseif (in_array($b, static::$envOrder)) {
			return array_search($b, static::$envOrder);
		}
		return strcasecmp($a, $b);
	}

	protected static function update() {
		$path = self::path();
		if (!is_dir($path)) {
			self::cloneRepo();
		}
	}

	protected static function cloneRepo() {
		exec("sudo git clone " . self::$repoUrl . " " . self::path(), $output);
	}

	protected static function path($repoPath = false) {
		return \Storage::disk('local')->path(self::$folder . ($repoPath ? '/' . $repoPath : ''));
	}

	protected static function getConfig($file) {
		return \Storage::disk('local')->get($file);
	}

	public static function getProjectTypes() {
		if (\Cache::get('deployConfigProjectTypes')) {
			return \Cache::get('deployConfigProjectTypes');
		}
		$dir		 = new \RecursiveDirectoryIterator(static::path());
		$ite		 = new \RecursiveIteratorIterator($dir);
		$files		 = new \RegexIterator($ite, "/^.+\_config.yml/i", \RegexIterator::GET_MATCH);
		$fileList	 = array();
		foreach ($files as $file) {
			$fileList = array_merge($fileList, $file);
		}
		$projectTypes = [];
		foreach ($fileList as $file) {
			$file = str_replace(self::path(), self::$folder, $file);
			try {
				$config = Yaml::parse(static::getConfig($file));
			} catch (\Symfony\Component\Yaml\Exception\ParseException $ex) {
				$config = [];
			}
			if (!empty($config['project_type']) && !in_array($config['project_type'], $projectTypes)) {
				$projectTypes[] = $config['project_type'];
			}
		}
		$projectTypes = array_combine($projectTypes, $projectTypes);
		ksort($projectTypes);
		\Cache::forever('deployConfigProjectTypes', $projectTypes);
		return $projectTypes;
	}

	public static function getBuildTasks() {
		if (\Cache::get('deployConfigBuildTasks')) {
			return \Cache::get('deployConfigBuildTasks');
		}
		$dir		 = new \RecursiveDirectoryIterator(static::path());
		$ite		 = new \RecursiveIteratorIterator($dir);
		$files		 = new \RegexIterator($ite, "/^.+\_config.yml/i", \RegexIterator::GET_MATCH);
		$fileList	 = array();
		foreach ($files as $file) {
			$fileList = array_merge($fileList, $file);
		}
		$buildTasks = [];
		foreach ($fileList as $file) {
			$file = str_replace(self::path(), self::$folder, $file);
			try {
				$config = Yaml::parse(static::getConfig($file));
			} catch (\Symfony\Component\Yaml\Exception\ParseException $ex) {
				$config = [];
			}
			if (!empty($config['build_tasks']) && is_array($config['build_tasks'])) {
				foreach ($config['build_tasks'] as $task) {
					if (!isset($buildTasks[$task])) {
						$buildTasks[$task] = $task;
					}
				}
			}
		}
		ksort($buildTasks);
		\Cache::forever('deployConfigBuildTasks', $buildTasks);
		return $buildTasks;
	}

}
