<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {

	public $validationRules = [
		'name' => 'required',
		'folder' => 'required',
		'domain' => 'required',
		'type_id' => 'required'
	];
	protected $setupClass;
	protected $environment;

	public function type() {
		return $this->hasOne('App\Project\Type', 'id', 'type_id');
	}

	public function group() {
		return $this->hasOne('App\Project\Group', 'id', 'group_id');
	}

	public function environment() {
		if (is_null($this->environment)) {
			if ($this->mainDomain() && $this->mainDomain()->environment_id) {
				$this->environment = Environment::find($this->mainDomain()->environment_id);
			} else {
				$this->environment = Environment::find($this->type->default_env);
			}
		}
		return $this->environment;
	}

	public function domains() {
		return $this->hasMany('App\Project\Domain');
	}
	
	/**
	 * 
	 * @return Project\Domain
	 */
	public function mainDomain() {
		return Project\Domain::where(['project_id' => $this->id, 'main' => 1])->first();
	}

	public function link($method = 'show', $params = []) {
		$params = array_merge(['id' => $this->id], $params);
		return route('projects.' . $method, $params);
	}

	public function directory() {
		return Settings::project('root_dir') . DIRECTORY_SEPARATOR . $this->group->folder . DIRECTORY_SEPARATOR . $this->folder;
	}

	public function publicDir() {
		$publicDir = $this->directory();
		if (is_dir($publicDir . '/web')) {
			$publicDir .= '/web';
		}
		if (is_dir($publicDir . '/public')) {
			$publicDir .= '/public';
		}
		return $publicDir;
	}

	public function isSetup() {
		//check if repo has been cloned
		if (!is_dir($this->directory())) {
			return false;
		}
		//check if hosts entry is made
		if (gethostbyname($this->mainDomain()->domain) != $this->environment()->ip_address) {
			return false;
		}
		
		return true;
	}

	public function setupClass() {
		if (is_null($this->setupClass)) {
			$this->setupClass = new \App\Project\Setup($this);
		}
		return $this->setupClass;
	}

	public function apacheFileName() {
		return 'project.' . $this->id;
	}

	public function apacheFile() {
		$env_files = [];

		$publicDir = $this->publicDir();
		$virtualHost = "";
		$non_ssl = $ssl = [];
		foreach ($this->domains as $domain) {
			$non_ssl[] = $domain;
			if ($domain->ssl) {
				$ssl[] = $domain;
			}
		}
		if ($non_ssl) {
			foreach ($non_ssl as $domain) {
				$virtualHost .= ($virtualHost ? "\n" : "") . "<VirtualHost {$domain->domain}:80>\nDocumentRoot $publicDir\n</VirtualHost>";
			}
		}
		if ($ssl) {
			$virtualHost .= ($virtualHost ? "\n" : "") . "<IfModule mod_ssl.c>";
			foreach ($ssl as $domain) {
				$virtualHost .= ($virtualHost ? "\n" : "") . "<VirtualHost {$domain->domain}:443>\nDocumentRoot $publicDir\nSSLEngine on\nSSLCertificateFile /etc/apache2/ssl/server.crt\nSSLCertificateKeyFile /etc/apache2/ssl/server.key\n</VirtualHost>";
			}
			$virtualHost .= ($virtualHost ? "\n" : "") . "</IfModule>";
		}
		return $virtualHost;
	}

	public function repoPath() {
		return str_replace(['git@git.d3corp.com:', '.git'], '', $this->repo_url);
	}
	
	public function openFolder(){
		shell_exec("/mnt/c/Windows/System32/cmd.exe /c start C:/Users/dukes/Documents/Lops/Projects/{$this->group->folder}/{$this->folder} > /dev/null 2>/dev/null &");
	}
	
	public function openNetbeansProject(){
		shell_exec("/mnt/c/Program\ Files/Netbeans\ 8.2/bin/netbeans64.exe --open C:/Users/dukes/Documents/Lops/Projects/{$this->group->folder}/{$this->folder} --console suppress > /dev/null 2>/dev/null &");
	}
	
	public function repoUrlParts(){
		$parts = [];
		$url = explode('@', $this->repo_url);
		$parts['user'] = $url[0];
		$url = $url[1];
		$url = explode(':', $url);
		$parts['domain'] = $url[0];
		$parts['url'] = str_replace('.git', '', $url[1]);
		return $parts;
	}
	public function repoWebUrl(){
		$repoUrlParts = $this->repoUrlParts();
		$url = 'https://'.$repoUrlParts['domain'].'/'.$repoUrlParts['url'];
		return $url;
	}
	
	public function opsUrl(){
		$repoUrlParts = $this->repoUrlParts();
		$url = 'http://ops.d3corp.com/projects/'.$repoUrlParts['url'];
		return $url;
	}
	
	public function openTerminal(){
		$winInst = str_replace('\\', '/', \App\DirTree::directoryToWindows($this->environment()->instanceFile()));
		shell_exec("cd {$this->directory()} && /mnt/c/Windows/System32/cmd.exe /c start {$winInst} > /dev/null 2>/dev/null &");
	}
	
	public function hasComposer(){
		return file_exists($this->directory() . '/composer.json');
	}
	
	public function updateComposer(){
		$this->mainDomain()->environment()->runCommand("cd {$this->directory()} && {$this->type->composer_cmd}");
	}
	
	public function apacheLogs(){
		$this->mainDomain()->environment()->runCommand("tail -f /var/log/apache2/error.log", false);
	}
	
	public function clearApacheLogs(){
		$this->mainDomain()->environment()->runCommand("echo '' > /var/log/apache2/error.log", false);
	}
}
