<?php

namespace App\Environment\Setup;

use App\Environment;

class Php
{
    protected static $versions = ['5.2', '5.6', '7.0', '7.1', '7.2'];
    public static $defaultExtensions = ['curl', 'gd', 'mbstring', 'mcrypt', 'mysql', 'xml', 'xmlrpc', 'zip', 'soap'];
    /** @var \App\Environment\Setup $setupClass */
    public static $setupClass = null;

    public static function versions()
    {
        return self::$versions;
    }

    /**
     * @param $version
     * @return array
     */
    public static function extensions($version)
    {
        if (in_array($version, self::$versions)) {
            $output = shell_exec("apt-cache pkgnames | grep '^php{$version}-' 2>&1");
            if ($output) {
                $exts = explode("\n", trim($output));
                asort($exts);
                return array_combine($exts, $exts);
            }
        }
        return [];
    }

    public static function defaultExtensions($version)
    {
        $defaultExtensions = self::$defaultExtensions;
        array_walk($defaultExtensions, function (&$value, $key) use ($version) {
            $value = "php{$version}-" . $value;
        });
        return $defaultExtensions;
    }

    public static function install(Environment $environment)
    {
        self::$setupClass->setCurrentMsg('Installing PHP');
        $environment->runCommand('sudo add-apt-repository ppa:ondrej/php -y');
        $environment->runCommand('sudo apt-get update');
        $method = 'install' . preg_replace("/[^0-9]/", "", $environment->php_version);
        if (method_exists(get_called_class(), $method)) {
            self::$method($environment);
        }


        self::$setupClass->setCurrentMsg('Installing PHP Extensions');
        self::installExtensions($environment);
        self::$setupClass->addMsg('Successfully Installed PHP Extensions');

        self::$setupClass->setCurrentMsg('Installing Composer');
        self::installComposer($environment);
        self::$setupClass->addMsg('Successfully Installed Composer');
    }

    private static function installComposer($environment){
        $environment->runCommand("sudo apt-get install -y composer");
    }

    public static function installExtensions(Environment $environment)
    {
        $exts = explode(',', $environment->php_extensions);
        foreach ($exts as $ext) {
            static::installExtension($environment, $ext);
        }
    }

    public static function installExtension(Environment $environment, $ext)
    {
        $environment->runCommand("sudo apt-get install -y $ext");
    }
    private static function install56(Environment $environment)
    {
        $environment->runCommand('sudo apt install -y php5.6 libapache2-mod-php5.6');
        self::$setupClass->addMsg('Successfully Installed PHP');

        self::$setupClass->setCurrentMsg('Configuring PHP');
        self::configure56($environment);
        self::$setupClass->addMsg('Successfully Configured PHP');
    }

    private static function configure56(Environment $environment){
        $environment->runCommand("sed -i 's/short_open_tag = Off/short_open_tag = On/g' /etc/php/5.6/apache2/php.ini");
    }
}