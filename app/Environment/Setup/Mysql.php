<?php

namespace App\Environment\Setup;

use App\Environment;

class Mysql
{
    protected static $versions = ['5.6', '5.7'];
    /** @var \App\Environment\Setup $setupClass */
    public static $setupClass = null;

    public static function versions()
    {
        return self::$versions;
    }

    public static function install(Environment $environment){
        self::$setupClass->setCurrentMsg('Installing Mysql');
        $method = 'install' . preg_replace("/[^0-9]/", "", $environment->php_version);
        if (method_exists(get_called_class(), $method)) {
            self::$method($environment);
        }
        self::restart($environment);
    }

    private static function install56(Environment $environment){
        $environment->runCommand('sudo add-apt-repository \'deb http://archive.ubuntu.com/ubuntu trusty universe\' -y');
        $environment->runCommand('sudo apt-get update');
        $environment->runCommand('DEBIAN_FRONTEND=noninteractive sudo -E apt-get -q -y install mysql-server-5.6');
        $environment->runCommand('sudo service mysql start');
        $environment->runCommand('mysqladmin -u root password root');
        self::$setupClass->addMsg('Successfully Installed Mysql');
        self::$setupClass->setCurrentMsg('Configuring Mysql');
        self::configure56($environment);
        self::$setupClass->addMsg('Successfully Configured Mysql');
    }

    private static function configure56(Environment $environment){
        $storagePath = rtrim(\Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix(), '/');
        $storagePath .= "/mysql/mysql5.6.cnf";
        $environment->runCommand("cat $storagePath > /etc/mysql/my.cnf");
        $environment->runCommand("sed -i 's/{{ENVIRONMENT_IP}}/{$environment->ip_address}/g' /etc/mysql/my.cnf");
        $environment->runCommand("mysql -uroot -proot -e 'CREATE USER \\\"root\\\"@\\\"{$environment->ip_address}\\\" IDENTIFIED BY \\\"root\\\";'");
        $environment->runCommand("mysql -uroot -proot -e 'GRANT ALL PRIVILEGES ON *.* TO \\\"root\\\"@\\\"{$environment->ip_address}\\\" WITH GRANT OPTION;'");
    }

    public static function restart(Environment $environment){
        $environment->runCommand('service mysql restart');
    }
}