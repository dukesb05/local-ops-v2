<?php
/**
 * Created by PhpStorm.
 * User: dukes
 * Date: 6/23/2018
 * Time: 9:06 AM
 */

namespace App\Environment\Setup;


class Hyperv
{

    protected static $netAdapters = null;
    protected static $netAdapter = null;

    public static function newNatIp($ipAddress, $prefixLength = 24){
        $netAdapter = self::netAdapter();
        $ifIndex = $netAdapter['ifIndex'];
        $cmd = "New-NetIPAddress -IPAddress $ipAddress -PrefixLength $prefixLength -InterfaceIndex $ifIndex";
        self::runCommand($cmd);
        self::newNat($prefixLength);
    }

    public static function newNat($prefixLength){
        $cmd = "New-NetNat -Name Lops -InternalIPInterfaceAddressPrefix 172.16.0.0/$prefixLength";
        self::runCommand($cmd);
    }

    /**
     * @return bool
     */
    public static function netAdapter(){
        if(is_null(self::$netAdapter)){
            foreach(self::netAdapters() as $netAdapter){
                if(strpos($netAdapter['InterfaceDescription'], 'Hyper-V') !== false && strpos($netAdapter['Name'], '(Default') !== false){
                    self::$netAdapter = $netAdapter;
                    return $netAdapter;
                }
            }
        }

        return self::$netAdapter;
    }
    /**
     * @return array
     */
    public static function netAdapters()
    {
        if(is_null(self::$netAdapters)) {
            $output = self::runCommand("Get-NetAdapter");
            if ($output) {
                $columnPositions = [26, 40, 8, 13, 23, 9];
                $lines = explode("\n", $output);
                unset($lines[1]);
                $headers = [];
                self::$netAdapters = [];
                foreach ($lines as $i => $row) {
                    $columns = self::getTerminalColsByPositions($row, $columnPositions);
                    if ($i == 0) {
                        $headers = $columns;
                    } else {
                        $netAdapter = [];
                        foreach ($columns as $j => $column) {
                            $netAdapter[$headers[$j]] = $column;
                        }
                        self::$netAdapters[] = $netAdapter;
                    }
                }
            }
        }
        return self::$netAdapters;
    }

    public static function getTerminalColsByPositions($row, $positions){
        $parts = array();

        foreach ($positions as $position){
            $parts[] = trim(substr($row, 0, $position));
            $row = substr($row, $position);
        }

        return $parts;
    }

    private static function runCommand($cmd){
        return trim(shell_exec("/mnt/c/Windows/System32/cmd.exe /c powershell -command \"$cmd\""));
    }
}