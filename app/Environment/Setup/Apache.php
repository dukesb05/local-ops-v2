<?php
/**
 * Created by PhpStorm.
 * User: dukes
 * Date: 6/23/2018
 * Time: 7:02 AM
 */

namespace App\Environment\Setup;

use App\Environment;
use App\Settings;

class Apache
{
    public static $defaultMods = ['headers', 'rewrite', 'expires', 'ssl'];
    /** @var \App\Environment\Setup $setupClass */
    public static $setupClass = null;
    /**
     * @return array
     */
    public static function availableMods()
    {
        $ignored = ['php7.1', 'php7.0'];
        $output = shell_exec("cd /etc/apache2/mods-available/ && ls *.load");
        if ($output) {
            $exts = explode("\n", trim($output));
            foreach ($exts as $i => $ext) {
                $exts[$i] = str_replace('.load', '', $ext);
                if (in_array($exts[$i], $ignored)) {
                    unset($exts[$i]);
                }
            }
            asort($exts);
            return array_combine($exts, $exts);
        }
        return [];
    }

    public static function install(Environment $environment)
    {
        self::$setupClass->setCurrentMsg('Installing Apache');
        $environment->runCommand('sudo apt-get install -y apache2');
        self::$setupClass->addMsg('Successfully Installed Apache');

        self::$setupClass->setCurrentMsg('Configuring Apache');
        self::configure($environment);
        self::$setupClass->addMsg('Successfully Configured Apache');

        self::$setupClass->setCurrentMsg('Enabling Apache Modules');
        self::enableMods($environment);
        self::$setupClass->addMsg('Successfully Enabled Apache Modules');

        self::$setupClass->setCurrentMsg('Restarting Apache');
        self::restart($environment);
    }

    public static function restart(Environment $environment){
        $environment->runCommand('service apache2 restart');
    }

    public static function configure(Environment $environment)
    {
        $environment->runCommand('sudo mkdir /etc/apache2/ssl');
        shell_exec("sudo ssh root@{$environment->ip_address} \"sudo openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -subj '/C=US/ST=Maryland/L=Ocean City/O=D3corp/CN=d3corp.com' -keyout /etc/apache2/ssl/server.key  -out /etc/apache2/ssl/server.crt\"");
        $environment->runCommand("sed -i 's/Listen 80/Listen {$environment->ip_address}:80/g' /etc/apache2/ports.conf");
        $environment->runCommand("sed -i 's/Listen 443/Listen {$environment->ip_address}:443/g' /etc/apache2/ports.conf");
        $storagePath = rtrim(\Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix(), '/');
        $storagePath .= "/apache/apache.conf";
        $environment->runCommand("cat $storagePath > /etc/apache2/apache2.conf");
        $project_root_dir = str_replace('/', "\/", Settings::project('root_dir'));
        $environment->runCommand("sed -i 's/{{PROJECT_ROOT_DIR}}/{$project_root_dir}/g' /etc/apache2/apache2.conf");
    }

    public static function enableMods(Environment $environment)
    {
        $mods = explode(',', $environment->apache_mods);
        foreach ($mods as $mod) {
            static::enableMod($environment, $mod);
        }
    }

    public static function enableMod(Environment $environment, $mod)
    {
        $environment->runCommand("sudo a2enmod $mod");
    }
}