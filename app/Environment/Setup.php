<?php

namespace App\Environment;

use App\ResourceHacker;
use App\Setup as BaseSetup;
use App\Environment;

class Setup extends BaseSetup
{
    protected $steps = [
        'install_environment',
        'install_apache',
        'install_php',
        //'install_mysql'
    ];

    private $environment;

    public function __construct(Environment $environment)
    {
        $this->environment = $environment;
    }

    public function getSteps()
    {
        return $this->steps;
    }

    public function run()
    {
        foreach ($this->steps as $step) {
            $this->$step();
        }

        $this->setStatus('completed');
    }

    /**
     *
     */
    public function install_environment()
    {
        $this->installEnvFiles();
        $this->renameEnv();
        $this->installEnv();
        $this->updateEnv();
        $this->setupNetwork();
        $this->copySshKeys();
        $this->setEnvIcon();
    }

    public function install_apache()
    {
        Environment\Setup\Apache::$setupClass = $this;
        Environment\Setup\Apache::install($this->environment);
    }

    public function install_php()
    {
        Environment\Setup\Php::$setupClass = $this;
        //Environment\Setup\Php::install($this->environment);
    }

    public function install_mysql()
    {
        Environment\Setup\Mysql::$setupClass = $this;
        //Environment\Setup\Mysql::install($this->environment);
    }

    protected function setEnvIcon(){
        $this->environment->generateIcon();
        ResourceHacker::addIcon($this->environment, $this->environment->folder().'/icon.ico');
    }

    protected function copySshKeys(){
        //first add git.d3corp.com to known hosts
        $this->setCurrentMsg('Adding D3 Git Repository to Known Hosts');
        $this->environment->runCommand('mkdir /root/.ssh && chmod 700 /root/.ssh');
        $this->environment->runCommand('touch /root/.ssh/known_hosts && chmod 644 /root/.ssh/known_hosts');
        $this->environment->runCommand('ssh-keyscan git.d3corp.com >> ~/.ssh/known_hosts');
        $this->addMsg("Successfully Added D3 Git Repository to Known Hosts");

        $this->setCurrentMsg('Enabling SSH on Env');
        $this->environment->runCommand("sed -i 's/\#ListenAddress 0\.0\.0\.0/ListenAddress {$this->environment->ip_address}/g' /etc/ssh/sshd_config");
        $this->environment->runCommand("sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config");
        $this->environment->runCommand("sudo dpkg-reconfigure openssh-server");
        $this->environment->runCommand("service ssh restart");
        shell_exec("sudo ssh-keygen -f '/root/.ssh/known_hosts' -R {$this->environment->ip_address} 2>&1");
        $out = shell_exec("sudo bash -c 'ssh-keyscan {$this->environment->ip_address} >> /root/.ssh/known_hosts' 2>&1");

        $ssh_pub_key = trim(shell_exec("sudo cat /root/.ssh/id_rsa.pub 2>&1"));
        \Storage::disk('local')->put("ssh_key/pub.key", $ssh_pub_key);
        $storagePath = rtrim(\Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix(), '/');
        $storagePath .= "/ssh_key/pub.key";
        $this->environment->runCommand('touch /root/.ssh/authorized_keys && chmod 600 /root/.ssh/authorized_keys');
        $this->environment->runCommand("cat $storagePath >> /root/.ssh/authorized_keys");
        $this->environment->runCommand("chmod 600 /root/.ssh/authorized_keys");
        $this->addMsg("Successfully Enabled SSH on Env");
        $this->setCurrentMsg('Copying Ssh Keys');
        shell_exec("sudo scp /root/.ssh/id_rsa /root/.ssh/id_rsa.pub root@{$this->environment->ip_address}:~/.ssh 2>&1");
        $this->addMsg("Successfully Copied Ssh Keys");
    }

    protected function updateEnv(){
        $this->setCurrentMsg('Updating Environment');
        $this->environment->runCommand('apt-get update');
        $this->environment->runCommand('apt-get upgrade -y');
        $this->addMsg("Successfully Updated Environment");
    }
    protected function setupNetwork(){
        $this->setCurrentMsg('Adding Env IP to local Hyper-v NAT');
        Environment\Setup\Hyperv::newNatIp($this->environment->ip_address);
        $this->addMsg("Successfully Added Env IP to local Hyper-v NAT");
    }

    protected function installEnv()
    {
        $this->setCurrentMsg('Building Environment');
        if (!is_dir($this->environment->folder() . '/rootfs')) {
            $this->environment->runCommand();
        }
        $this->addMsg("Successfully Built Environment");
    }

    protected function renameEnv()
    {
        $this->setCurrentMsg('Renaming Instance');
        if (!file_exists($this->environment->instanceFile())) {
            exec("sudo mv {$this->environment->folder()}/InstanceName.exe {$this->environment->instanceFile()}", $output);
        }
        $this->addMsg('Successfully Renamed Instance');
    }

    protected function installEnvFiles()
    {
        $this->setCurrentMsg('Extracting Environment Files');
        if (!is_dir($this->environment->folder())) {
            exec("sudo git clone https://gitlab.com/dukesb05/windows-ubuntu-env.git {$this->environment->folder()}", $output);
        }
        $this->addMsg("Successfully Extracted Environment Files to {$this->environment->folder()}");
    }
}