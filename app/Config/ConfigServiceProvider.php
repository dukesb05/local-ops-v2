<?php

namespace App\Config;

use Illuminate\Support\ServiceProvider;

class ConfigServiceProvider extends ServiceProvider {

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register() {
		// Bind it only once so we can reuse in IoC
		$this->app->singleton('App\Config\Repository', function($app, $items) {
			print_r($items);
			$writer = new FileWriter($app['files'], $app['path.config']);
			return new Repository($items, $writer);
		});
		
		$this->app->extend('config', function ($command, $app) {
			$writer = new FileWriter($app['files'], $app['path.config']);
            return new Repository($command->all(), $writer);
        });

		// Capture the loaded configuration items
		$this->app->alias('config', \App\Config\Repository::class);
	}

}
