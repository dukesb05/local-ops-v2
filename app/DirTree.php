<?php

namespace App;

class DirTree {

	protected static $directories = [];
	public static $startDir;
	protected static $rootDir = '/mnt';
	
	public static function getDirs($directory) {
		$directories = \File::directories($directory);
		$dirs = [];
		foreach ($directories as $directory) {
			$dir = ['ubuntu' => $directory, 'win' => self::directoryToWindows($directory)];
			if (strpos(self::$startDir, $dir['ubuntu']) !== false) {
				$dir['subDirs'] = self::getDirs($dir['ubuntu']);
			}
			$dirs[] = $dir;
		}
		self::$directories = $dirs;
		return $dirs;
	}

	public static function directories($startDir, $rootDir = '/mnt') {
		self::$startDir = $startDir;
		self::getDirs($rootDir);
		return self::$directories;
	}

	public static function folderHtmlId($directory) {
		return str_replace('/', '-', $directory);
	}

	public static function directoryToWindows($directory) {
		$dirs = explode('/', ltrim($directory, '/'));
		if (count($dirs) < 2) {
			return '';
		}
		$windows_dir = strtoupper($dirs[1]) . ':\\';
		unset($dirs[0], $dirs[1]);
		if ($dirs) {
			$windows_dir .= implode('\\', $dirs);
		}
		return $windows_dir;
	}

	public static function subDirs($directory, $htmlId) {
		if (!empty($directory['subDirs'])) {
			$html = '<div class="list-group show" id="' . DirTree::folderHtmlId($directory['ubuntu']) . '">';
			foreach ($directory['subDirs'] as $directory) {
				$html .= view('form.dirTree.tree')->with('directory', $directory)->with('htmlId', DirTree::folderHtmlId($directory['ubuntu']))->with('value', self::directoryToWindows(self::$startDir));
			}
			$html .= '</div>';
			return $html;
		}
		return '<div class="list-group collapse hide" id="' . $htmlId . '"></div>';
	}
	

}
