<?php
/**
 * Created by PhpStorm.
 * User: dukes
 * Date: 6/23/2018
 * Time: 7:02 PM
 */

namespace App;


class ResourceHacker
{
    public static function addIcon(Environment $environment, $icon){
        self::installedCheck();
        $windowsFolder = str_replace('\\', '\\\\', \App\DirTree::directoryToWindows($environment->folder()));
        $exe = str_replace('\\', '\\\\', \App\DirTree::directoryToWindows(rtrim(\Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix(), '/').'/resourcehacker/ResourceHacker.exe'));
        $output = shell_exec("/mnt/c/Windows/System32/cmd.exe /c $exe -open $windowsFolder/{$environment->cleanName()}.exe -save $windowsFolder/{$environment->cleanName()}.exe -action addoverwrite -res $windowsFolder/icon.ico -mask ICONGROUP,MAINICON, 2>&1");
    }

    public static function installedCheck(){
        if(!self::isInstalled()){
            self::install();
        }
    }

    public static function isInstalled(){
        $storagePath = rtrim(\Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix(), '/').'/resourcehacker';
        $file = "resource_hacker.zip";
        if(!file_exists($storagePath.'/'.$file)){
            return false;
        }
        return true;
    }

    private static function install(){
        $storagePath = rtrim(\Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix(), '/').'/resourcehacker';
        $file = "resource_hacker.zip";
        shell_exec("mkdir $storagePath && wget -O $storagePath/$file http://www.angusj.com/resourcehacker/resource_hacker.zip");
        shell_exec("unzip $storagePath/$file -d $storagePath");
    }
}